# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 201201010070400) do

  create_table "account_heads", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                   null: false
    t.integer  "parent_id",         limit: 4
    t.string   "name",              limit: 255,                 null: false
    t.string   "desc",              limit: 255
    t.string   "relevance",         limit: 100
    t.integer  "created_by",        limit: 4,                   null: false
    t.integer  "approved_by",       limit: 4
    t.boolean  "deleted",                       default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "erasable",                      default: true
  end

  add_index "account_heads", ["company_id"], name: "acc_head_comp_idx", using: :btree

  create_table "account_histories", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                        null: false
    t.integer  "account_id",        limit: 4,                                        null: false
    t.integer  "financial_year_id", limit: 4,                                        null: false
    t.decimal  "opening_balance",             precision: 18, scale: 2, default: 0.0
    t.decimal  "closing_balance",             precision: 18, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                            null: false
    t.integer  "account_head_id",   limit: 4,                                            null: false
    t.string   "name",              limit: 255,                                          null: false
    t.integer  "accountable_id",    limit: 4
    t.string   "accountable_type",  limit: 255
    t.decimal  "opening_balance",               precision: 18, scale: 2, default: 0.0
    t.integer  "created_by",        limit: 4,                                            null: false
    t.integer  "approved_by",       limit: 4
    t.boolean  "deleted",                                                default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id",         limit: 4
    t.integer  "customer_id",       limit: 4
    t.integer  "vendor_id",         limit: 4
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "erasable",                                               default: true
  end

  add_index "accounts", ["company_id", "name"], name: "acc_comp_idx", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.string  "address_line1",    limit: 255
    t.string  "address_line2",    limit: 255
    t.string  "city",             limit: 100
    t.string  "state",            limit: 100
    t.string  "country",          limit: 100
    t.string  "postal_code",      limit: 7
    t.integer "addressable_id",   limit: 4
    t.string  "addressable_type", limit: 255
    t.integer "address_type",     limit: 4,   default: 1
  end

  create_table "announcements", force: :cascade do |t|
    t.integer  "company_id", limit: 4
    t.integer  "plan_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "subject",    limit: 255
    t.text     "message",    limit: 65535
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", force: :cascade do |t|
    t.integer  "company_id",    limit: 4,     null: false
    t.integer  "user_id",       limit: 4,     null: false
    t.string   "asset_tag",     limit: 255
    t.text     "description",   limit: 65535
    t.date     "purchase_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "assigned_to",   limit: 4
  end

  create_table "assignments", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attendances", force: :cascade do |t|
    t.integer  "company_id",   limit: 4
    t.integer  "user_id",      limit: 4
    t.date     "month"
    t.decimal  "days_present",           precision: 4, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "days_absent",            precision: 4, scale: 2, default: 0.0
  end

  create_table "auditor_assignments", force: :cascade do |t|
    t.integer  "auditor_id", limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "auditors", force: :cascade do |t|
    t.string   "first_name",      limit: 100, null: false
    t.string   "last_name",       limit: 100, null: false
    t.string   "username",        limit: 32,  null: false
    t.string   "hashed_password", limit: 255, null: false
    t.string   "salt",            limit: 255, null: false
    t.boolean  "reset_password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",       limit: 4
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "account_number", limit: 50,  null: false
    t.string   "bank_name",      limit: 255, null: false
    t.string   "rtgs_code",      limit: 25
    t.string   "micr_code",      limit: 25
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ifsc_code",      limit: 255
  end

  create_table "bank_statement_headers", force: :cascade do |t|
    t.integer  "bank_id",     limit: 4
    t.integer  "company_id",  limit: 4
    t.string   "header_1",    limit: 255
    t.string   "header_2",    limit: 255
    t.string   "header_3",    limit: 255
    t.string   "header_4",    limit: 255
    t.string   "header_5",    limit: 255
    t.string   "header_6",    limit: 255
    t.string   "header_7",    limit: 255
    t.string   "header_8",    limit: 255
    t.string   "date_format", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bank_statement_line_items", force: :cascade do |t|
    t.date     "date"
    t.string   "from_account",           limit: 255
    t.string   "to_account",             limit: 255
    t.decimal  "amount",                               precision: 18, scale: 2
    t.text     "description",            limit: 65535
    t.integer  "status",                 limit: 4
    t.integer  "bank_statement_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id",             limit: 4
    t.boolean  "credit_debit_indicator"
    t.integer  "company_id",             limit: 4
    t.decimal  "account_balance",                      precision: 18, scale: 2
    t.integer  "ledger_id",              limit: 4
    t.date     "value_date"
    t.string   "cheque_reference",       limit: 255
  end

  create_table "bank_statements", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.integer  "created_by",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.integer  "status",            limit: 4
    t.integer  "account_id",        limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
  end

  create_table "billing_invoice_sequences", force: :cascade do |t|
  end

  create_table "billing_invoices", force: :cascade do |t|
    t.integer  "company_id",     limit: 4
    t.string   "invoice_number", limit: 255,                                        null: false
    t.datetime "invoice_date",                                                      null: false
    t.decimal  "amount",                     precision: 18, scale: 2, default: 0.0
    t.integer  "created_by",     limit: 4,                                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status_id",      limit: 4,                            default: 0,   null: false
    t.string   "received_by",    limit: 255
    t.integer  "closed_by",      limit: 4
  end

  create_table "billing_line_items", force: :cascade do |t|
    t.integer "company_id",         limit: 4,                                          null: false
    t.integer "billing_invoice_id", limit: 4,                                          null: false
    t.string  "line_item",          limit: 255,                                        null: false
    t.decimal "amount",                         precision: 18, scale: 2, default: 0.0
    t.string  "billing_type",       limit: 255,                                        null: false
    t.decimal "validity",                       precision: 10, scale: 2
  end

  create_table "blog_categories", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blog_category_blog_posts", force: :cascade do |t|
    t.integer "blog_category_id", limit: 4, null: false
    t.integer "blog_post_id",     limit: 4, null: false
  end

  create_table "blog_posts", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.text     "content",          limit: 65535
    t.integer  "super_user_id",    limit: 4
    t.integer  "status",           limit: 4
    t.integer  "view_count",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "blog_category_id", limit: 4
    t.text     "slug",             limit: 65535
    t.string   "description",      limit: 255
    t.string   "author",           limit: 255
  end

  create_table "branches", force: :cascade do |t|
    t.integer  "company_id", limit: 4,                   null: false
    t.string   "name",       limit: 255,                 null: false
    t.string   "phone",      limit: 15
    t.string   "fax",        limit: 15
    t.integer  "created_by", limit: 4,                   null: false
    t.boolean  "deleted",                default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "campaign_name", limit: 255
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "capital_accounts", force: :cascade do |t|
    t.string "name",           limit: 255
    t.text   "address",        limit: 65535
    t.string "city",           limit: 255
    t.string "state",          limit: 255
    t.string "PIN",            limit: 255
    t.string "PAN",            limit: 255
    t.string "sales_tax_no",   limit: 255
    t.string "service_tax_no", limit: 255
    t.string "email",          limit: 255
  end

  create_table "cash_accounts", force: :cascade do |t|
    t.string   "location",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cash_free_settings", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,   null: false
    t.integer  "account_id",          limit: 4,   null: false
    t.string   "app_id",              limit: 255, null: false
    t.string   "secret_key",          limit: 255, null: false
    t.string   "status",              limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "expense_account",     limit: 4
    t.integer  "expense_tax_account", limit: 4
  end

  create_table "cashfree_documents", force: :cascade do |t|
    t.integer  "company_id",                       limit: 4
    t.integer  "created_by",                       limit: 4
    t.string   "uploaded_file_one_file_name",      limit: 255
    t.string   "uploaded_file_one_content_type",   limit: 255
    t.integer  "uploaded_file_one_file_size",      limit: 4
    t.datetime "uploaded_file_one_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_two_file_name",      limit: 255
    t.string   "uploaded_file_two_content_type",   limit: 255
    t.integer  "uploaded_file_two_file_size",      limit: 4
    t.datetime "uploaded_file_two_updated_at"
    t.string   "uploaded_file_three_file_name",    limit: 255
    t.string   "uploaded_file_three_content_type", limit: 255
    t.integer  "uploaded_file_three_file_size",    limit: 4
    t.datetime "uploaded_file_three_updated_at"
    t.string   "name",                             limit: 255
    t.string   "pan",                              limit: 255
  end

  create_table "cashfree_payment_links", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,   null: false
    t.integer  "invoice_id",     limit: 4,   null: false
    t.string   "order_id",       limit: 255, null: false
    t.string   "order_amount",   limit: 255, null: false
    t.string   "order_note",     limit: 255
    t.string   "customer_name",  limit: 255
    t.integer  "customer_phone", limit: 4,   null: false
    t.string   "customer_email", limit: 255
    t.integer  "seller_phone",   limit: 4
    t.string   "shorturl",       limit: 255
    t.string   "created_by",     limit: 255, null: false
    t.string   "status",         limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id",    limit: 4
  end

  create_table "cashfree_responses", force: :cascade do |t|
    t.string   "order_id",     limit: 255,                                        null: false
    t.decimal  "order_amount",             precision: 18, scale: 2, default: 0.0
    t.string   "reference_id", limit: 255,                                        null: false
    t.string   "tx_status",    limit: 255
    t.string   "payment_mode", limit: 255
    t.string   "tx_message",   limit: 255,                                        null: false
    t.string   "tx_time",      limit: 255,                                        null: false
    t.string   "signature",    limit: 255,                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remote_ip",    limit: 100
  end

  create_table "channels", force: :cascade do |t|
    t.string   "channel_name", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",                    limit: 255,                                                  null: false
    t.string   "subdomain",               limit: 255,                                                  null: false
    t.string   "pan",                     limit: 10
    t.string   "sales_tax_no",            limit: 100
    t.string   "tin",                     limit: 100
    t.date     "activation_date",                                                                      null: false
    t.date     "expiry_date"
    t.boolean  "reminder"
    t.integer  "active",                  limit: 3
    t.boolean  "deleted",                               default: false
    t.datetime "deleted_datetime"
    t.string   "deleted_by",              limit: 255
    t.text     "deleted_reason",          limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone",                   limit: 100
    t.string   "fax",                     limit: 100
    t.string   "email",                   limit: 255
    t.string   "VAT_no",                  limit: 255
    t.string   "CST_no",                  limit: 255
    t.string   "excise_reg_no",           limit: 255
    t.string   "service_tax_reg_no",      limit: 255
    t.integer  "setup_progress",          limit: 4
    t.boolean  "subscription_type",                     default: true
    t.integer  "status",                  limit: 4
    t.string   "logo_file_name",          limit: 255
    t.string   "logo_content_type",       limit: 255
    t.integer  "logo_file_size",          limit: 4
    t.datetime "logo_updated_at"
    t.integer  "payroll_date",            limit: 4
    t.text     "terms_and_conditions",    limit: 65535
    t.text     "customer_note",           limit: 65535
    t.string   "watermark",               limit: 150,   default: "Generated from www.profitbooks.net"
    t.integer  "country_id",              limit: 4
    t.string   "timezone",                limit: 255
    t.string   "tan_no",                  limit: 255
    t.string   "lbt_registration_number", limit: 255
    t.string   "facebook_url",            limit: 255
    t.string   "twitter_url",             limit: 255
    t.string   "linked_in_url",           limit: 255
    t.string   "google_plus_url",         limit: 255
    t.integer  "you_sell",                limit: 4
    t.integer  "business_type",           limit: 4
    t.integer  "industry",                limit: 4
    t.integer  "total_employees",         limit: 4
    t.integer  "source",                  limit: 4
    t.integer  "current_system",          limit: 4
    t.integer  "annual_turnover",         limit: 4
    t.integer  "ca_status",               limit: 4
  end

  add_index "companies", ["subdomain"], name: "cmp_subdomain_idx", using: :btree

  create_table "company_auditors", force: :cascade do |t|
    t.integer  "company_id",       limit: 4, null: false
    t.integer  "auditor_id",       limit: 4, null: false
    t.integer  "department_id",    limit: 4
    t.integer  "designation_id",   limit: 4
    t.integer  "reporting_to_id",  limit: 4
    t.datetime "last_login_at"
    t.boolean  "deleted"
    t.datetime "deleted_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "company_currencies", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.integer  "currency_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "company_notes", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "company_templates", force: :cascade do |t|
    t.integer  "company_id",   limit: 4,   null: false
    t.integer  "template_id",  limit: 4,   null: false
    t.string   "voucher_type", limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,     null: false
    t.integer  "created_by",       limit: 4,     null: false
    t.string   "title",            limit: 255
    t.string   "first_name",       limit: 100
    t.string   "last_name",        limit: 100
    t.string   "email",            limit: 100
    t.string   "gender",           limit: 255
    t.date     "birthday"
    t.date     "anniversary"
    t.text     "address1",         limit: 65535
    t.text     "address2",         limit: 65535
    t.string   "city",             limit: 255
    t.string   "state",            limit: 255
    t.string   "pin_code",         limit: 255
    t.string   "country",          limit: 255
    t.string   "phone1",           limit: 10
    t.string   "phone2",           limit: 10
    t.string   "mobile",           limit: 10
    t.string   "business_fax",     limit: 15
    t.string   "contact_category", limit: 255
    t.string   "designation",      limit: 255
    t.string   "department",       limit: 255
    t.string   "company",          limit: 255
    t.string   "account",          limit: 255
    t.text     "notes",            limit: 65535
    t.string   "web_page",         limit: 255
    t.string   "thumbnail",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",          limit: 4
    t.integer  "sundry_debtor_id", limit: 4
    t.string   "role",             limit: 50
    t.string   "position",         limit: 50
    t.string   "previous_company", limit: 50
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",             limit: 80
    t.string   "isd_code",         limit: 5
    t.string   "currency_unicode", limit: 10
    t.string   "currency_code",    limit: 3
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "country_companies", force: :cascade do |t|
    t.integer  "company_id", limit: 4
    t.integer  "country_id", limit: 4
    t.string   "timezone",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "coupon_transactions", force: :cascade do |t|
    t.integer  "coupon_id",  limit: 4
    t.integer  "company_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "used"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "coupons", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "coupon_code",       limit: 255
    t.string   "coupon_type",       limit: 255
    t.decimal  "discount",                      precision: 10
    t.decimal  "total_amount",                  precision: 10
    t.date     "date_start"
    t.date     "date_end"
    t.integer  "uses_per_coupon",   limit: 4
    t.integer  "uses_per_customer", limit: 4
    t.string   "status",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "year",                          precision: 10
  end

  create_table "credit_note_sequences", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,             null: false
    t.integer  "credit_note_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "credit_notes", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,                                              null: false
    t.integer  "created_by",         limit: 4,                                              null: false
    t.string   "credit_note_number", limit: 255,                                            null: false
    t.date     "transaction_date",                                                          null: false
    t.decimal  "amount",                           precision: 18, scale: 2, default: 0.0,   null: false
    t.integer  "from_account_id",    limit: 4
    t.integer  "to_account_id",      limit: 4,                                              null: false
    t.text     "description",        limit: 65535
    t.boolean  "deleted",                                                   default: false
    t.integer  "deleted_by",         limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",     limit: 255
    t.integer  "restored_by",        limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",          limit: 4
    t.boolean  "opened",                                                    default: true
    t.integer  "invoice_return_id",  limit: 4
    t.boolean  "read_only",                                                 default: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "currency_code", limit: 3,  null: false
    t.string   "symbol",        limit: 10
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "current_assets", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "current_liabilities", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "custom_fields", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,                   null: false
    t.string   "voucher_type",   limit: 255,                 null: false
    t.string   "custom_label1",  limit: 255
    t.string   "custom_label2",  limit: 255
    t.string   "custom_label3",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status",                     default: false
    t.string   "default_value1", limit: 255
    t.string   "default_value2", limit: 255
    t.string   "default_value3", limit: 255
  end

  create_table "customer_imports", force: :cascade do |t|
    t.integer  "import_file_id",          limit: 4
    t.string   "name",                    limit: 255
    t.string   "opening_balance",         limit: 255
    t.string   "currency",                limit: 255
    t.string   "phone_number",            limit: 255
    t.string   "email",                   limit: 255
    t.string   "website",                 limit: 255
    t.string   "pan",                     limit: 255
    t.string   "tan",                     limit: 255
    t.string   "vat_tin",                 limit: 255
    t.string   "cst_reg_no",              limit: 255
    t.string   "cin",                     limit: 255
    t.string   "excise_reg_no",           limit: 255
    t.string   "service_tax_reg_no",      limit: 255
    t.string   "lbt_registration_number", limit: 255
    t.string   "credit_days",             limit: 255
    t.string   "credit_limit",            limit: 255
    t.string   "billing_address",         limit: 255
    t.string   "city",                    limit: 255
    t.string   "state",                   limit: 255
    t.string   "country",                 limit: 255
    t.string   "postal_code",             limit: 255
    t.string   "shipping_address",        limit: 255
    t.integer  "created_by",              limit: 4
    t.integer  "status",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customer_relationships", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.text     "notes",             limit: 65535
    t.date     "last_contact_date"
    t.date     "next_contact_date"
    t.integer  "created_by",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "notable_id",        limit: 4
    t.string   "notable_type",      limit: 255
    t.integer  "activity",          limit: 4
    t.date     "record_date"
    t.decimal  "time_spent",                      precision: 10
    t.boolean  "activity_status",                                default: false
    t.string   "next_folloup_time", limit: 255
    t.integer  "next_activity",     limit: 4
    t.date     "completed_date"
  end

  create_table "customers", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,                                null: false
    t.string   "name",                     limit: 255,                              null: false
    t.string   "phone_number",             limit: 255
    t.string   "fax",                      limit: 255
    t.string   "email",                    limit: 255
    t.string   "website",                  limit: 255
    t.string   "pan",                      limit: 25
    t.string   "tan",                      limit: 25
    t.string   "vat_tin",                  limit: 25
    t.string   "cst_reg_no",               limit: 25
    t.string   "cin",                      limit: 25
    t.string   "excise_reg_no",            limit: 25
    t.string   "service_tax_reg_no",       limit: 25
    t.integer  "product_pricing_level_id", limit: 4
    t.string   "country",                  limit: 100
    t.string   "weekly_off",               limit: 50
    t.string   "open_time",                limit: 10
    t.string   "close_time",               limit: 10
    t.string   "bank_name",                limit: 255
    t.string   "ifsc_code",                limit: 25
    t.string   "micr_code",                limit: 25
    t.string   "bsr_code",                 limit: 25
    t.date     "incorporated_date"
    t.integer  "credit_days",              limit: 4,                    default: 0
    t.decimal  "credit_limit",                           precision: 10, default: 0
    t.integer  "created_by",               limit: 4,                                null: false
    t.text     "background_info",          limit: 65535
    t.text     "notes",                    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sales_tax_no",             limit: 4
    t.string   "lbt_registration_number",  limit: 255
    t.integer  "currency_id",              limit: 4
  end

  create_table "debit_note_sequences", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,             null: false
    t.integer  "debit_note_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "debit_notes", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,                                              null: false
    t.integer  "created_by",         limit: 4,                                              null: false
    t.string   "debit_note_number",  limit: 255,                                            null: false
    t.date     "transaction_date",                                                          null: false
    t.decimal  "amount",                           precision: 18, scale: 2, default: 0.0,   null: false
    t.integer  "from_account_id",    limit: 4
    t.integer  "to_account_id",      limit: 4,                                              null: false
    t.text     "description",        limit: 65535
    t.boolean  "deleted",                                                   default: false
    t.integer  "deleted_by",         limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",     limit: 255
    t.integer  "restored_by",        limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",          limit: 4
    t.boolean  "opened",                                                    default: true
    t.integer  "purchase_return_id", limit: 4
    t.boolean  "read_only",                                                 default: false
  end

  create_table "deferred_tax_asset_or_liabilities", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0
    t.integer  "attempts",   limit: 4,     default: 0
    t.text     "handler",    limit: 65535
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "delivery_challan_line_items", force: :cascade do |t|
    t.integer  "delivery_challan_id",      limit: 4,                              null: false
    t.integer  "product_id",               limit: 4,                              null: false
    t.decimal  "quantity",                               precision: 10, scale: 2
    t.text     "description",              limit: 65535
    t.integer  "product_batch_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sales_order_line_item_id", limit: 4
  end

  create_table "delivery_challan_sequences", force: :cascade do |t|
    t.integer  "company_id",                limit: 4
    t.integer  "delivery_challan_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delivery_challans", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,                 null: false
    t.integer  "created_by",           limit: 4,                 null: false
    t.integer  "sales_order_id",       limit: 4,                 null: false
    t.integer  "account_id",           limit: 4
    t.integer  "warehouse_id",         limit: 4
    t.string   "voucher_number",       limit: 255,               null: false
    t.date     "voucher_date",                                   null: false
    t.integer  "status",               limit: 4,     default: 0
    t.text     "customer_notes",       limit: 65535
    t.text     "terms_and_conditions", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id",          limit: 4
  end

  create_table "departments", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.integer  "user_id",     limit: 4
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "status",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deposit_accounts", force: :cascade do |t|
    t.boolean "interest_applicable"
    t.decimal "interest_rate",                 precision: 4, scale: 2
    t.integer "compounding_type",    limit: 4
  end

  create_table "deposit_sequences", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,             null: false
    t.integer  "deposit_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deposits", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                            null: false
    t.integer  "created_by",        limit: 4,                                            null: false
    t.string   "voucher_number",    limit: 255
    t.date     "transaction_date"
    t.integer  "from_account_id",   limit: 4,                                            null: false
    t.integer  "to_account_id",     limit: 4,                                            null: false
    t.decimal  "amount",                        precision: 18, scale: 2, default: 0.0
    t.string   "description",       limit: 255
    t.boolean  "deleted",                                                default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",         limit: 4
  end

  create_table "designations", force: :cascade do |t|
    t.integer  "company_id",  limit: 4,     null: false
    t.string   "title",       limit: 255,   null: false
    t.text     "description", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "direct_expense_accounts", force: :cascade do |t|
    t.boolean "inventoriable"
  end

  create_table "direct_income_accounts", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "duties_and_taxes_accounts", force: :cascade do |t|
    t.integer  "tax_id",               limit: 4
    t.decimal  "tax_rate",                           precision: 4, scale: 2
    t.boolean  "auto_calculate_tax"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "filling_frequency",    limit: 4
    t.string   "registration_number",  limit: 255
    t.integer  "apply_to",             limit: 4
    t.text     "description",          limit: 65535
    t.decimal  "calculate_on_percent",               precision: 5, scale: 2, default: 100.0
    t.integer  "calculation_method",   limit: 4,                             default: 0
  end

  create_table "email_templates", force: :cascade do |t|
    t.string   "template_name", limit: 255
    t.string   "subject",       limit: 255
    t.string   "header",        limit: 255
    t.string   "body",          limit: 255
    t.string   "footer",        limit: 255
    t.integer  "created_by",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",    limit: 4
    t.string   "email",         limit: 255
  end

  create_table "employee_goals", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,     null: false
    t.integer  "for_employee",      limit: 4,     null: false
    t.integer  "created_by",        limit: 4,     null: false
    t.integer  "goals",             limit: 4
    t.date     "from_date",                       null: false
    t.date     "to_date",                         null: false
    t.text     "employee_comments", limit: 65535
    t.text     "manager_comments",  limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estimate_histories", force: :cascade do |t|
    t.integer  "estimate_id", limit: 4
    t.integer  "company_id",  limit: 4
    t.string   "description", limit: 255
    t.integer  "created_by",  limit: 4
    t.datetime "record_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estimate_line_items", force: :cascade do |t|
    t.integer  "estimate_id",    limit: 4,                                            null: false
    t.integer  "account_id",     limit: 4
    t.decimal  "quantity",                     precision: 10, scale: 2
    t.decimal  "unit_rate",                    precision: 18, scale: 4
    t.decimal  "discount",                     precision: 4,  scale: 2
    t.boolean  "tax"
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0
    t.text     "description",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "line_item_type", limit: 255
    t.integer  "product_id",     limit: 4
    t.integer  "tax_account_id", limit: 4
  end

  create_table "estimate_sequences", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,             null: false
    t.integer  "estimate_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estimate_taxes", force: :cascade do |t|
    t.integer  "estimate_line_item_id", limit: 4, null: false
    t.integer  "account_id",            limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estimates", force: :cascade do |t|
    t.integer  "company_id",              limit: 4,                                              null: false
    t.integer  "account_id",              limit: 4,                                              null: false
    t.integer  "created_by",              limit: 4,                                              null: false
    t.string   "estimate_number",         limit: 255
    t.date     "estimate_date",                                                                  null: false
    t.text     "customer_notes",          limit: 65535
    t.text     "terms_and_conditions",    limit: 65535
    t.integer  "status",                  limit: 4
    t.boolean  "deleted",                                                        default: false
    t.integer  "deleted_by",              limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",          limit: 255
    t.integer  "restored_by",             limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "due_date"
    t.integer  "branch_id",               limit: 4
    t.string   "custom_field1",           limit: 255
    t.string   "custom_field2",           limit: 255
    t.string   "custom_field3",           limit: 255
    t.decimal  "total_amount",                          precision: 18, scale: 2, default: 0.0
    t.string   "attachment_file_name",    limit: 255
    t.string   "attachment_content_type", limit: 255
    t.integer  "attachment_file_size",    limit: 4
    t.datetime "attachment_updated_at"
    t.integer  "currency_id",             limit: 4
    t.decimal  "exchange_rate",                         precision: 18, scale: 5, default: 0.0
    t.boolean  "tax_inclusive",                                                  default: false
  end

  create_table "expense_line_items", force: :cascade do |t|
    t.integer  "expense_id",  limit: 4,                                          null: false
    t.integer  "account_id",  limit: 4,                                          null: false
    t.decimal  "amount",                  precision: 18, scale: 2, default: 0.0
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "tax"
    t.string   "type",        limit: 255
  end

  create_table "expense_sequences", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,             null: false
    t.integer  "expense_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "expense_taxes", force: :cascade do |t|
    t.integer  "expense_line_item_id", limit: 4, null: false
    t.integer  "account_id",           limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "expenses", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,                                              null: false
    t.integer  "account_id",                 limit: 4,                                              null: false
    t.integer  "created_by",                 limit: 4,                                              null: false
    t.string   "voucher_number",             limit: 25
    t.date     "expense_date"
    t.text     "customer_notes",             limit: 65535
    t.text     "tags",                       limit: 65535
    t.boolean  "deleted",                                                           default: false
    t.integer  "deleted_by",                 limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",             limit: 255
    t.integer  "restored_by",                limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.integer  "project_id",                 limit: 4
    t.integer  "branch_id",                  limit: 4
    t.decimal  "total_amount",                             precision: 18, scale: 2, default: 0.0
    t.boolean  "credit_expense",                                                    default: false
    t.date     "due_date"
    t.integer  "status_id",                  limit: 4,                              default: 1
    t.integer  "currency_id",                limit: 4
    t.decimal  "exchange_rate",                            precision: 18, scale: 5, default: 0.0
  end

  create_table "expenses_payments", force: :cascade do |t|
    t.integer  "payment_voucher_id", limit: 4
    t.integer  "expense_id",         limit: 4
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0
    t.decimal  "tds_amount",                   precision: 18, scale: 2, default: 0.0
    t.boolean  "deleted",                                               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "company_id",   limit: 4,     null: false
    t.integer  "submitted_by", limit: 4
    t.string   "vote",         limit: 255
    t.text     "comment",      limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financial_year_logs", force: :cascade do |t|
    t.integer  "company_id",           limit: 4
    t.integer  "financial_year_id",    limit: 4
    t.datetime "activity_on"
    t.integer  "activity",             limit: 4
    t.decimal  "past_opening_balance",           precision: 10, default: 0
    t.decimal  "past_closing_balance",           precision: 10, default: 0
    t.integer  "created_by",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financial_years", force: :cascade do |t|
    t.integer "company_id",              limit: 4,                                          null: false
    t.integer "year_id",                 limit: 4,                                          null: false
    t.boolean "freeze",                                                     default: false
    t.date    "start_date",                                                                 null: false
    t.date    "end_date",                                                                   null: false
    t.decimal "opening_balance",                   precision: 18, scale: 2, default: 0.0
    t.decimal "closing_balance",                   precision: 18, scale: 2
    t.decimal "opening_stock_valuation",           precision: 18, scale: 2, default: 0.0
    t.decimal "closing_stock_valuation",           precision: 18, scale: 2, default: 0.0
  end

  create_table "finished_goods", force: :cascade do |t|
    t.integer  "company_id",    limit: 4, null: false
    t.integer  "user_id",       limit: 4
    t.integer  "inventory_id",  limit: 4, null: false
    t.integer  "recieved_from", limit: 4
    t.integer  "quantity",      limit: 4
    t.date     "recieved_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fixed_assets", force: :cascade do |t|
    t.boolean  "depreciable"
    t.decimal  "depreciation_rate", precision: 8, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "folders", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.integer  "parent_id",  limit: 4
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "folders", ["parent_id"], name: "index_folders_on_parent_id", using: :btree
  add_index "folders", ["user_id"], name: "index_folders_on_user_id", using: :btree

  create_table "grants", force: :cascade do |t|
    t.integer  "right_id",   limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "helps", force: :cascade do |t|
    t.string   "screen_name", limit: 255
    t.integer  "screen_id",   limit: 4
    t.string   "help",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "holidays", force: :cascade do |t|
    t.date     "holiday_date",               null: false
    t.string   "holiday",      limit: 255,   null: false
    t.text     "description",  limit: 65535
    t.integer  "created_by",   limit: 4,     null: false
    t.integer  "company_id",   limit: 4,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "import_files", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,   null: false
    t.integer  "item_type",         limit: 4,   null: false
    t.integer  "created_by",        limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.integer  "status",            limit: 4
  end

  create_table "income_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",              limit: 4,             null: false
    t.integer  "income_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "income_vouchers", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                              null: false
    t.integer  "created_by",        limit: 4,                                              null: false
    t.string   "voucher_number",    limit: 32,                                             null: false
    t.date     "income_date",                                                              null: false
    t.integer  "from_account_id",   limit: 4,                                              null: false
    t.integer  "to_account_id",     limit: 4,                                              null: false
    t.decimal  "amount",                          precision: 18, scale: 2, default: 0.0,   null: false
    t.text     "description",       limit: 65535
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",         limit: 4
  end

  create_table "indirect_expense_accounts", force: :cascade do |t|
    t.boolean "inventoriable"
  end

  create_table "indirect_income_accounts", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instamojo_payment_links", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,   null: false
    t.integer  "invoice_id",         limit: 4,   null: false
    t.string   "payment_request_id", limit: 255, null: false
    t.string   "customer_name",      limit: 255
    t.string   "purpose",            limit: 255, null: false
    t.string   "amount",             limit: 255, null: false
    t.string   "longurl",            limit: 255, null: false
    t.string   "shorturl",           limit: 255
    t.string   "created_by",         limit: 255, null: false
    t.string   "status",             limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id",        limit: 4,   null: false
  end

  create_table "instamojo_payments", force: :cascade do |t|
    t.integer  "company_id",   limit: 4,     null: false
    t.string   "account_name", limit: 255,   null: false
    t.string   "api_key",      limit: 255,   null: false
    t.string   "auth_key",     limit: 255,   null: false
    t.string   "status",       limit: 255,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "salt_key",     limit: 65535, null: false
    t.integer  "account_id",   limit: 4,     null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "company_id",      limit: 4,   null: false
    t.integer  "user_id",         limit: 4,   null: false
    t.integer  "account_id",      limit: 4,   null: false
    t.integer  "cutoff_level1",   limit: 4
    t.integer  "cutoff_level2",   limit: 4
    t.integer  "quantity",        limit: 4
    t.string   "unit_of_measure", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inventory_settings", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,                  null: false
    t.boolean  "purchase_effects_inventory",            default: false, null: false
    t.string   "inventory_model",            limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "investment_accounts", force: :cascade do |t|
    t.boolean "inventoriable"
  end

  create_table "invitation_details", force: :cascade do |t|
    t.integer  "company_id", limit: 4,               null: false
    t.integer  "sent_by",    limit: 4,               null: false
    t.string   "email",      limit: 100,             null: false
    t.string   "token",      limit: 255,             null: false
    t.integer  "status_id",  limit: 4,   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       limit: 255
  end

  create_table "invoice_attachments", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,   null: false
    t.integer  "created_by",                 limit: 4,   null: false
    t.string   "invoice_id",                 limit: 255, null: false
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_credit_allocations", force: :cascade do |t|
    t.integer  "invoice_id",     limit: 4
    t.integer  "credit_note_id", limit: 4
    t.decimal  "amount",                   precision: 18, scale: 2
    t.boolean  "deleted",                                           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_histories", force: :cascade do |t|
    t.integer  "invoice_id",  limit: 4
    t.integer  "company_id",  limit: 4
    t.string   "description", limit: 255
    t.integer  "created_by",  limit: 4
    t.datetime "record_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_line_items", force: :cascade do |t|
    t.integer  "invoice_id",       limit: 4,                                            null: false
    t.integer  "account_id",       limit: 4
    t.decimal  "quantity",                       precision: 10, scale: 2
    t.decimal  "unit_rate",                      precision: 18, scale: 4
    t.decimal  "discount_percent",               precision: 5,  scale: 2, default: 0.0
    t.string   "cost_center",      limit: 255
    t.boolean  "tax"
    t.decimal  "amount",                         precision: 18, scale: 2, default: 0.0
    t.text     "description",      limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",             limit: 255
    t.integer  "warehouse_id",     limit: 4
    t.integer  "product_id",       limit: 4
    t.integer  "tax_account_id",   limit: 4
    t.integer  "task_id",          limit: 4
  end

  add_index "invoice_line_items", ["product_id"], name: "inv_ln_item_product_idx", using: :btree

  create_table "invoice_return_line_items", force: :cascade do |t|
    t.integer  "invoice_return_id",    limit: 4
    t.integer  "account_id",           limit: 4
    t.decimal  "quantity",                         precision: 10, scale: 2, default: 0.0
    t.decimal  "unit_rate",                        precision: 18, scale: 4, default: 0.0
    t.boolean  "tax",                                                       default: false
    t.decimal  "amount",                           precision: 18, scale: 2, default: 0.0
    t.string   "line_type",            limit: 255
    t.integer  "product_id",           limit: 4
    t.integer  "tax_account_id",       limit: 4
    t.decimal  "discount_percent",                 precision: 8,  scale: 2
    t.integer  "invoice_line_item_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoice_return_line_items", ["product_id"], name: "inv_rtrn_ln_item_product_idx", using: :btree

  create_table "invoice_return_sequences", force: :cascade do |t|
    t.integer  "company_id",              limit: 4
    t.integer  "invoice_return_sequence", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_return_taxes", force: :cascade do |t|
    t.integer  "invoice_return_line_item_id", limit: 4, null: false
    t.integer  "account_id",                  limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_returns", force: :cascade do |t|
    t.integer  "company_id",            limit: 4,                                            null: false
    t.integer  "invoice_id",            limit: 4,                                            null: false
    t.integer  "created_by",            limit: 4,                                            null: false
    t.string   "invoice_return_number", limit: 255
    t.integer  "account_id",            limit: 4,                                            null: false
    t.date     "record_date"
    t.text     "description",           limit: 65535
    t.decimal  "total_amount",                        precision: 18, scale: 2, default: 0.0
    t.integer  "currency_id",           limit: 4
    t.decimal  "exchange_rate",                       precision: 18, scale: 5, default: 0.0
    t.integer  "credit_note_id",        limit: 4
    t.integer  "warehouse_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",             limit: 4
  end

  create_table "invoice_sales_orders", force: :cascade do |t|
    t.integer  "invoice_id",     limit: 4, null: false
    t.integer  "sales_order_id", limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_settings", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,                     null: false
    t.integer  "invoice_sequence",    limit: 4,     default: 0
    t.integer  "invoice_no_strategy", limit: 4,     default: 0,     null: false
    t.string   "invoice_prefix",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invoice_suffix",      limit: 255
    t.string   "item_label",          limit: 255
    t.string   "desc_label",          limit: 255
    t.string   "qty_label",           limit: 255
    t.string   "rate_label",          limit: 255
    t.string   "discount_label",      limit: 255
    t.string   "amount_label",        limit: 255
    t.text     "invoice_footer",      limit: 65535
    t.boolean  "view_payment",                      default: true
    t.boolean  "enable_gateway",                    default: false
    t.boolean  "enable_cashfree",                   default: false
  end

  create_table "invoice_statuses", force: :cascade do |t|
    t.string   "status",      limit: 255, null: false
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_taxes", force: :cascade do |t|
    t.integer  "invoice_line_item_id", limit: 4, null: false
    t.integer  "account_id",           limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,                                              null: false
    t.integer  "account_id",               limit: 4
    t.integer  "created_by",               limit: 4,                                              null: false
    t.string   "invoice_number",           limit: 255
    t.date     "invoice_date",                                                                    null: false
    t.date     "due_date",                                                                        null: false
    t.string   "po_reference",             limit: 255
    t.text     "customer_notes",           limit: 65535
    t.text     "terms_and_conditions",     limit: 65535
    t.integer  "invoice_status_id",        limit: 4
    t.boolean  "deleted",                                                         default: false
    t.integer  "deleted_by",               limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",           limit: 255
    t.integer  "restored_by",              limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "cash_invoice",                                                    default: false
    t.string   "cash_customer_name",       limit: 255
    t.string   "cash_customer_mobile",     limit: 255
    t.string   "cash_customer_email",      limit: 255
    t.integer  "project_id",               limit: 4
    t.string   "custom_field1",            limit: 255
    t.string   "custom_field2",            limit: 255
    t.string   "custom_field3",            limit: 255
    t.integer  "branch_id",                limit: 4
    t.boolean  "time_invoice",                                                    default: false
    t.integer  "recursive_invoice",        limit: 4,                              default: 0
    t.decimal  "total_amount",                           precision: 18, scale: 2, default: 0.0
    t.integer  "voucher_title_id",         limit: 4
    t.integer  "so_invoice",               limit: 4,                              default: 0
    t.integer  "currency_id",              limit: 4
    t.decimal  "exchange_rate",                          precision: 18, scale: 5, default: 0.0
    t.boolean  "tax_inclusive",                                                   default: false
    t.integer  "settlement_account_id",    limit: 4
    t.decimal  "settlement_exchange_rate",               precision: 18, scale: 5, default: 0.0
    t.integer  "financial_year_id",        limit: 4
    t.integer  "estimate_id",              limit: 4
  end

  create_table "invoices_receipts", force: :cascade do |t|
    t.integer  "receipt_voucher_id", limit: 4
    t.integer  "invoice_id",         limit: 4
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0
    t.decimal  "tds_amount",                   precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",                                               default: false
  end

  create_table "issue_raw_materials", force: :cascade do |t|
    t.integer  "company_id",   limit: 4, null: false
    t.integer  "inventory_id", limit: 4, null: false
    t.integer  "issued_to",    limit: 4, null: false
    t.date     "issue_date"
    t.integer  "quantity",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: :cascade do |t|
    t.integer  "user_id",      limit: 4,     null: false
    t.integer  "company_id",   limit: 4,     null: false
    t.string   "title",        limit: 255,   null: false
    t.text     "description",  limit: 65535
    t.string   "status",       limit: 255
    t.date     "joining_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journal_import_line_items", force: :cascade do |t|
    t.integer  "journal_import_id", limit: 4
    t.integer  "from_account_id",   limit: 4
    t.decimal  "amount",                      precision: 18, scale: 2
    t.decimal  "credit_amount",               precision: 18, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journal_imports", force: :cascade do |t|
    t.integer  "import_file_id",  limit: 4
    t.string   "voucher_number",  limit: 255
    t.date     "date"
    t.integer  "created_by",      limit: 4
    t.text     "description",     limit: 65535
    t.string   "tags",            limit: 255
    t.decimal  "total_amount",                  precision: 18, scale: 2
    t.integer  "from_account_id", limit: 4
    t.decimal  "amount",                        precision: 18, scale: 2
    t.decimal  "credit_amount",                 precision: 18, scale: 2
    t.integer  "status",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journal_line_items", force: :cascade do |t|
    t.integer  "journal_id",      limit: 4,                                        null: false
    t.integer  "from_account_id", limit: 4,                                        null: false
    t.decimal  "amount",                    precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "credit_amount",             precision: 18, scale: 2, default: 0.0
  end

  create_table "journal_sequences", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,             null: false
    t.integer  "journal_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journal_to_line_items", force: :cascade do |t|
    t.integer  "journal_id",    limit: 4,                          null: false
    t.integer  "to_account_id", limit: 4,                          null: false
    t.decimal  "amount",                  precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journals", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                              null: false
    t.integer  "account_id",        limit: 4
    t.integer  "created_by",        limit: 4,                                              null: false
    t.date     "date"
    t.string   "voucher_number",    limit: 25
    t.text     "description",       limit: 65535
    t.string   "tags",              limit: 255
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",         limit: 4
    t.decimal  "total_amount",                    precision: 18, scale: 2, default: 0.0
    t.integer  "project_id",        limit: 4
  end

  create_table "labels", force: :cascade do |t|
    t.integer  "company_id",      limit: 4,   null: false
    t.integer  "created_by",      limit: 4,   null: false
    t.string   "estimate_label",  limit: 255
    t.string   "warehouse_label", limit: 255
    t.string   "customer_label",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lead_activities", force: :cascade do |t|
    t.integer  "lead_id",             limit: 4
    t.integer  "activity",            limit: 4
    t.datetime "record_date"
    t.decimal  "time_spent",                        precision: 10
    t.text     "outcome",             limit: 65535
    t.date     "next_followup"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "lead_activity",                                    default: false
    t.integer  "next_activity",       limit: 4
    t.boolean  "activity_status",                                  default: false
    t.string   "next_follow_up_time", limit: 255
    t.date     "completed_date"
  end

  create_table "lead_companies", force: :cascade do |t|
    t.integer  "lead_id",    limit: 4
    t.integer  "company_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "leads", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "mobile",             limit: 15
    t.string   "email",              limit: 255
    t.integer  "channel_id",         limit: 4
    t.integer  "campaign_id",        limit: 4
    t.date     "next_call_date"
    t.text     "description",        limit: 65535
    t.integer  "status",             limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city",               limit: 255
    t.string   "organisation_name",  limit: 255
    t.text     "notes",              limit: 65535
    t.decimal  "estimated_revenue",                precision: 10, scale: 2
    t.integer  "probability",        limit: 4
    t.integer  "plan_of_interest",   limit: 4
    t.boolean  "payment_status"
    t.integer  "created_by",         limit: 4
    t.boolean  "deleted",                                                   default: false
    t.integer  "deleted_by",         limit: 4
    t.integer  "deleted_reason",     limit: 4
    t.integer  "converted_comment",  limit: 4
    t.integer  "converted_by",       limit: 4
    t.integer  "lead_type",          limit: 4
    t.boolean  "converted_to_trial",                                        default: false
    t.integer  "assigned_to",        limit: 4
    t.integer  "stage",              limit: 4
    t.integer  "trial_status",       limit: 4
    t.integer  "paid_status",        limit: 4
    t.text     "segment",            limit: 65535
    t.text     "source",             limit: 65535
  end

  create_table "leave_cards", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,                                       null: false
    t.integer  "user_id",            limit: 4,                                       null: false
    t.integer  "leave_type_id",      limit: 4,                                       null: false
    t.integer  "card_year",          limit: 2,                                       null: false
    t.integer  "total_leave_cnt",    limit: 2,                         default: 0,   null: false
    t.decimal  "utilized_leave_cnt",           precision: 4, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "leave_requests", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,     null: false
    t.integer  "user_id",              limit: 4,     null: false
    t.integer  "approved_by",          limit: 4
    t.integer  "leave_type_id",        limit: 4,     null: false
    t.integer  "leave_status",         limit: 4
    t.date     "start_date"
    t.date     "end_date",                           null: false
    t.text     "reason_for_leave",     limit: 65535
    t.string   "contact_during_leave", limit: 10
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "approver_comment",     limit: 255
  end

  create_table "leave_types", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,                     null: false
    t.integer  "created_by",     limit: 4,                     null: false
    t.string   "leave_type",     limit: 255
    t.integer  "allowed_leaves", limit: 4
    t.text     "description",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "carry",                        default: false, null: false
    t.boolean  "paid",                         default: true
  end

  create_table "leaves", force: :cascade do |t|
    t.integer  "company_id",      limit: 4, null: false
    t.integer  "user_id",         limit: 4, null: false
    t.integer  "leave_type_id",   limit: 4, null: false
    t.date     "year"
    t.integer  "leaves_count",    limit: 4
    t.integer  "leaves_utilized", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ledgers", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,                                              null: false
    t.integer  "account_id",               limit: 4,                                              null: false
    t.integer  "created_by",               limit: 4,                                              null: false
    t.date     "transaction_date",                                                                null: false
    t.decimal  "debit",                                  precision: 18, scale: 2, default: 0.0
    t.decimal  "credit",                                 precision: 18, scale: 2, default: 0.0
    t.string   "voucher_number",           limit: 255,                                            null: false
    t.integer  "voucher_id",               limit: 4
    t.string   "voucher_type",             limit: 255
    t.date     "bank_transaction_date"
    t.text     "description",              limit: 65535
    t.boolean  "deleted",                                                         default: false
    t.integer  "deleted_by",               limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",           limit: 255
    t.integer  "restored_by",              limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",                limit: 4
    t.boolean  "reconcilation_status",                                            default: false
    t.date     "reconcilation_date"
    t.string   "correlate",                limit: 255
    t.integer  "corresponding_account_id", limit: 4
  end

  add_index "ledgers", ["company_id", "account_id", "transaction_date", "voucher_type", "branch_id"], name: "ledger_idx", using: :btree

  create_table "line_items", force: :cascade do |t|
    t.integer  "voucher_id",            limit: 4
    t.integer  "account_id",            limit: 4
    t.text     "description",           limit: 65535
    t.integer  "quantity",              limit: 4
    t.decimal  "unit_cost",                           precision: 10
    t.decimal  "amount",                              precision: 10
    t.text     "purchase_order_number", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loan_accounts", force: :cascade do |t|
    t.string   "account_number", limit: 50,                                        null: false
    t.string   "bank_name",      limit: 255,                                       null: false
    t.string   "loan_type",      limit: 255
    t.decimal  "interest_rate",              precision: 4, scale: 2, default: 0.0
    t.string   "details",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loans_advances_accounts", force: :cascade do |t|
    t.boolean "interest_applicable"
    t.decimal "interest_rate",                 precision: 4, scale: 2
    t.integer "compounding_type",    limit: 4
  end

  create_table "make_payments", force: :cascade do |t|
    t.date     "payment_date"
    t.decimal  "amount",                           precision: 10, scale: 2
    t.string   "payment_mode",       limit: 255
    t.integer  "from_account_id",    limit: 4
    t.integer  "paid_to_account_id", limit: 4
    t.string   "bill_ref",           limit: 255
    t.string   "description",        limit: 255
    t.binary   "send_notification",  limit: 65535
    t.integer  "company_id",         limit: 4
    t.integer  "user_id",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "master_objectives", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,     null: false
    t.integer  "department_id",  limit: 4,     null: false
    t.integer  "created_by",     limit: 4,     null: false
    t.string   "objective_name", limit: 255
    t.text     "details",        limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: :cascade do |t|
    t.string   "subject",     limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4,     null: false
    t.integer  "user_id",     limit: 4,     null: false
    t.integer  "company_id",  limit: 4,     null: false
    t.integer  "status",      limit: 4,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reply_id",    limit: 4
  end

  create_table "myfiles", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,   null: false
    t.integer  "user_id",                    limit: 4,   null: false
    t.integer  "folder_id",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
  end

  add_index "myfiles", ["folder_id"], name: "index_myfiles_on_folder_id", using: :btree
  add_index "myfiles", ["user_id"], name: "index_myfiles_on_user_id", using: :btree

  create_table "notes", force: :cascade do |t|
    t.integer  "company_id",  limit: 4,     null: false
    t.integer  "created_by",  limit: 4,     null: false
    t.string   "subject",     limit: 255
    t.text     "description", limit: 65535
    t.integer  "status",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organisation_announcements", force: :cascade do |t|
    t.integer  "company_id",   limit: 4,     null: false
    t.integer  "created_by",   limit: 4,     null: false
    t.string   "title",        limit: 255,   null: false
    t.text     "announcement", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "other_current_assets", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "paid_line_items", force: :cascade do |t|
    t.integer  "expense_id",  limit: 4,                                          null: false
    t.integer  "account_id",  limit: 4,                                          null: false
    t.decimal  "amount",                  precision: 18, scale: 2, default: 0.0
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pay_grades", force: :cascade do |t|
    t.integer  "user_id",     limit: 4,                                            null: false
    t.integer  "company_id",  limit: 4,                                            null: false
    t.integer  "job_id",      limit: 4,                                            null: false
    t.string   "grade_name",  limit: 255,                                          null: false
    t.text     "description", limit: 65535
    t.decimal  "amount",                    precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payheads", force: :cascade do |t|
    t.integer  "company_id",             limit: 4,                   null: false
    t.integer  "defined_by",             limit: 4,                   null: false
    t.string   "payhead_type",           limit: 255
    t.string   "payhead_name",           limit: 255
    t.string   "under",                  limit: 255
    t.string   "affect_net_salary",      limit: 255
    t.string   "name_appear_in_payslip", limit: 255
    t.string   "use_of_gratuity",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "optional",                           default: false
    t.integer  "account_id",             limit: 4,                   null: false
    t.integer  "calculation_type",       limit: 2
  end

  create_table "payment_details", force: :cascade do |t|
    t.integer  "voucher_id",            limit: 4,                                          null: false
    t.string   "voucher_type",          limit: 255,                                        null: false
    t.decimal  "amount",                            precision: 18, scale: 2, default: 0.0
    t.date     "payment_date",                                                             null: false
    t.string   "account_number",        limit: 16
    t.string   "bank_name",             limit: 255
    t.string   "transaction_reference", limit: 50
    t.string   "branch",                limit: 255
    t.string   "type",                  limit: 255,                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_gateways", force: :cascade do |t|
    t.string   "name",        limit: 255,                 null: false
    t.string   "key",         limit: 255,                 null: false
    t.string   "api_key",     limit: 255
    t.string   "vanity_url",  limit: 255,                 null: false
    t.string   "gateway_url", limit: 255,                 null: false
    t.boolean  "production",              default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_transactions", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4
    t.integer  "transaction_status",         limit: 4
    t.string   "transaction_reference",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "issuer_ref_num",             limit: 255
    t.string   "auth_ID_code",               limit: 255
    t.string   "transaction_msg",            limit: 255
    t.string   "gateway_transaction_num",    limit: 255
    t.string   "gateway_transaction_status", limit: 255
    t.string   "gateway_transaction_ref",    limit: 255
    t.integer  "billing_invoice_id",         limit: 4
    t.string   "signature",                  limit: 255
  end

  create_table "payment_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,             null: false
    t.integer  "payment_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_vouchers", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,                                              null: false
    t.integer  "purchase_id",                limit: 4
    t.integer  "created_by",                 limit: 4,                                              null: false
    t.string   "voucher_number",             limit: 32,                                             null: false
    t.date     "voucher_date",                                                                      null: false
    t.date     "payment_date",                                                                      null: false
    t.integer  "from_account_id",            limit: 4,                                              null: false
    t.integer  "to_account_id",              limit: 4,                                              null: false
    t.decimal  "amount",                                   precision: 18, scale: 2, default: 0.0,   null: false
    t.text     "description",                limit: 65535
    t.boolean  "deleted",                                                           default: false
    t.integer  "deleted_by",                 limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",             limit: 255
    t.integer  "restored_by",                limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.integer  "branch_id",                  limit: 4
    t.integer  "tds_account_id",             limit: 4
    t.decimal  "tds_amount",                               precision: 18, scale: 2, default: 0.0
    t.integer  "currency_id",                limit: 4
    t.decimal  "exchange_rate",                            precision: 18, scale: 5, default: 0.0
    t.integer  "expense_id",                 limit: 4
    t.boolean  "advanced",                                                          default: false
    t.boolean  "allocated"
    t.integer  "voucher_type",               limit: 4,                              default: 0
  end

  create_table "payments", force: :cascade do |t|
    t.date     "payment_date"
    t.decimal  "amount",                            precision: 10, scale: 2
    t.string   "payment_mode",          limit: 255
    t.integer  "cheque_number",         limit: 4
    t.date     "cheque_date"
    t.string   "bank",                  limit: 255
    t.string   "branch",                limit: 255
    t.integer  "transaction_id",        limit: 4
    t.date     "card_transaction_date"
    t.string   "type_of_credit_card",   limit: 255
    t.string   "card_number",           limit: 255
    t.integer  "from_account_id",       limit: 4
    t.integer  "paid_to_account_id",    limit: 4
    t.string   "bill_ref",              limit: 255
    t.string   "description",           limit: 255
    t.integer  "ledger_id",             limit: 4
    t.string   "voucher_number",        limit: 255
    t.integer  "company_id",            limit: 4
    t.integer  "created_by",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",             limit: 4
  end

  create_table "payroll_details", force: :cascade do |t|
    t.integer  "company_id", limit: 4
    t.date     "month"
    t.integer  "status",     limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by", limit: 4
  end

  create_table "payroll_execution_jobs", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.date     "execution_date"
    t.boolean  "status",                      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id",        limit: 4
    t.integer  "payroll_detail_id", limit: 4
  end

  create_table "payroll_settings", force: :cascade do |t|
    t.integer  "company_id",               limit: 4
    t.boolean  "enable_payslip_signatory",             default: false
    t.string   "payslip_footer",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pbreferrals", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,                                          null: false
    t.integer  "invited_by",         limit: 4,                                          null: false
    t.integer  "coupon_id",          limit: 4,                                          null: false
    t.string   "name",               limit: 255
    t.string   "email",              limit: 100,                                        null: false
    t.integer  "status",             limit: 4,                            default: 0
    t.string   "token",              limit: 255,                                        null: false
    t.decimal  "earning",                        precision: 18, scale: 2, default: 0.0
    t.integer  "invitee_company_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plan_properties", force: :cascade do |t|
    t.integer  "plan_id",    limit: 4
    t.string   "name",       limit: 50
    t.string   "value",      limit: 50
    t.string   "datatype",   limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.decimal  "price",                          precision: 18, scale: 2, default: 0.0
    t.text     "description",      limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "display_name",     limit: 255
    t.integer  "user_count",       limit: 4
    t.decimal  "storage_limit_mb",               precision: 10
    t.boolean  "active",                                                  default: false
  end

  create_table "policy_documents", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,   null: false
    t.integer  "user_id",                    limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
  end

  add_index "policy_documents", ["company_id"], name: "index_policy_documents_on_company_id", using: :btree
  add_index "policy_documents", ["user_id"], name: "index_policy_documents_on_user_id", using: :btree

  create_table "process_payrolls", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "company_id", limit: 4
    t.string   "month",      limit: 255
    t.integer  "attendance", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_batches", force: :cascade do |t|
    t.integer  "product_id",                 limit: 4
    t.integer  "warehouse_id",               limit: 4
    t.string   "batch_number",               limit: 255
    t.decimal  "quantity",                               precision: 10, scale: 2, default: 0.0
    t.date     "manufacture_date"
    t.date     "expiry_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",                 limit: 4
    t.string   "reference",                  limit: 255
    t.integer  "stock_receipt_line_item_id", limit: 4
    t.decimal  "opening_stock_unit_price",               precision: 18, scale: 2
    t.boolean  "opening_batch",                                                   default: false
  end

  create_table "product_histories", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.integer  "product_id",        limit: 4
    t.integer  "financial_year_id", limit: 4
    t.decimal  "opening_stock",               precision: 18, scale: 2, default: 0.0
    t.decimal  "unit_value",                  precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_imports", force: :cascade do |t|
    t.integer  "import_file_id",  limit: 4
    t.string   "name",            limit: 255
    t.string   "description",     limit: 255
    t.string   "warehouse",       limit: 255
    t.string   "quantity",        limit: 255
    t.string   "batch_no",        limit: 255
    t.string   "unit_price",      limit: 255
    t.string   "reorder_level",   limit: 255
    t.string   "unit_of_measure", limit: 255
    t.string   "sales_price",     limit: 255
    t.string   "income_account",  limit: 255
    t.string   "purchase_price",  limit: 255
    t.string   "expense_account", limit: 255
    t.integer  "status",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_pricing_levels", force: :cascade do |t|
    t.integer  "company_id",       limit: 4
    t.string   "caption",          limit: 255
    t.decimal  "discount_percent",             precision: 5, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_settings", force: :cascade do |t|
    t.integer  "company_id",         limit: 4
    t.boolean  "multilevel_pricing",           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: :cascade do |t|
    t.integer  "company_id",         limit: 4,                                              null: false
    t.integer  "account_id",         limit: 4
    t.integer  "created_by",         limit: 4,                                              null: false
    t.string   "name",               limit: 255,                                            null: false
    t.string   "unit_of_measure",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "average_price",                    precision: 10,           default: 0
    t.integer  "branch_id",          limit: 4
    t.text     "description",        limit: 65535
    t.string   "type",               limit: 255
    t.decimal  "purchase_price",                   precision: 18, scale: 2, default: 0.0
    t.decimal  "sales_price",                      precision: 18, scale: 2, default: 0.0
    t.integer  "expense_account_id", limit: 4
    t.integer  "income_account_id",  limit: 4
    t.decimal  "reorder_level",                    precision: 10, scale: 2, default: 0.0
    t.boolean  "inventory"
    t.boolean  "batch_enable",                                              default: false
    t.string   "product_code",       limit: 255
    t.date     "as_on"
  end

  create_table "profitbooks_workstreams", force: :cascade do |t|
    t.integer "feature_id",   limit: 4,     null: false
    t.string  "icon_code",    limit: 150,   null: false
    t.date    "release_date",               null: false
    t.string  "title",        limit: 255,   null: false
    t.text    "details",      limit: 65535
    t.string  "link_URL",     limit: 255
    t.integer "created_by",   limit: 2,     null: false
    t.integer "status",       limit: 1,     null: false
  end

  create_table "projects", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,                                              null: false
    t.integer  "created_by",     limit: 4,                                              null: false
    t.string   "name",           limit: 255
    t.date     "start_date"
    t.date     "end_date"
    t.decimal  "estimated_cost",               precision: 18, scale: 2, default: 0.0
    t.text     "description",    limit: 65535
    t.boolean  "status",                                                default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "closed_by",      limit: 4
    t.integer  "branch_id",      limit: 4
  end

  create_table "provision_accounts", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_accounts", force: :cascade do |t|
    t.boolean  "inventoriable"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description",      limit: 65535
    t.decimal  "unit_cost",                      precision: 10, scale: 2, default: 0.0
    t.boolean  "reseller_product",                                        default: false
    t.decimal  "sell_cost",                      precision: 10, scale: 2, default: 0.0
  end

  create_table "purchase_attachments", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,   null: false
    t.integer  "user_id",                    limit: 4,   null: false
    t.string   "purchase_id",                limit: 255, null: false
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_debit_allocations", force: :cascade do |t|
    t.integer  "purchase_id",   limit: 4
    t.integer  "debit_note_id", limit: 4
    t.decimal  "amount",                  precision: 18, scale: 2
    t.boolean  "deleted",                                          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_line_items", force: :cascade do |t|
    t.integer  "purchase_id",              limit: 4,                                              null: false
    t.integer  "account_id",               limit: 4
    t.decimal  "quantity",                               precision: 18, scale: 2, default: 0.0
    t.decimal  "unit_rate",                              precision: 18, scale: 4
    t.string   "purchase_order_reference", limit: 255
    t.string   "cost_center",              limit: 255
    t.boolean  "tax",                                                             default: false
    t.decimal  "amount",                                 precision: 18, scale: 2, default: 0.0
    t.text     "description",              limit: 65535
    t.boolean  "deleted"
    t.integer  "deleted_by",               limit: 4
    t.integer  "approved_by",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",                     limit: 255
    t.integer  "warehouse_id",             limit: 4
    t.integer  "product_id",               limit: 4
    t.integer  "tax_account_id",           limit: 4
    t.decimal  "discount_percent",                       precision: 5,  scale: 2
  end

  add_index "purchase_line_items", ["product_id"], name: "pur_ln_item_product_idx", using: :btree

  create_table "purchase_order_line_items", force: :cascade do |t|
    t.integer  "purchase_order_id", limit: 4
    t.integer  "account_id",        limit: 4
    t.decimal  "quantity",                        precision: 10, scale: 2
    t.decimal  "unit_rate",                       precision: 18, scale: 4
    t.string   "cost_center",       limit: 255
    t.boolean  "tax"
    t.decimal  "amount",                          precision: 18, scale: 2
    t.text     "description",       limit: 65535
    t.boolean  "deleted"
    t.boolean  "deleted_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "line_item_type",    limit: 255
    t.integer  "product_id",        limit: 4
    t.integer  "tax_account_id",    limit: 4
    t.decimal  "discount_percent",                precision: 5,  scale: 2
  end

  create_table "purchase_order_sequences", force: :cascade do |t|
    t.integer  "company_id",              limit: 4,             null: false
    t.integer  "purchase_order_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_order_taxes", force: :cascade do |t|
    t.integer  "purchase_order_line_item_id", limit: 4, null: false
    t.integer  "account_id",                  limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_orders", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,                                              null: false
    t.integer  "created_by",                 limit: 4,                                              null: false
    t.string   "purchase_order_number",      limit: 255
    t.integer  "account_id",                 limit: 4,                                              null: false
    t.date     "po_date"
    t.date     "record_date",                                                                       null: false
    t.string   "bill_reference",             limit: 255
    t.integer  "status",                     limit: 4
    t.boolean  "deleted",                                                           default: false
    t.integer  "deleted_by",                 limit: 4
    t.text     "customer_notes",             limit: 65535
    t.text     "terms_and_conditions",       limit: 65535
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",             limit: 255
    t.integer  "restored_by",                limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.integer  "branch_id",                  limit: 4
    t.decimal  "total_amount",                             precision: 18, scale: 2, default: 0.0
    t.integer  "currency_id",                limit: 4
    t.decimal  "exchange_rate",                            precision: 18, scale: 5, default: 0.0
    t.integer  "project_id",                 limit: 4
  end

  create_table "purchase_return_line_items", force: :cascade do |t|
    t.integer  "purchase_return_id",    limit: 4,                                            null: false
    t.integer  "account_id",            limit: 4
    t.decimal  "quantity",                          precision: 10, scale: 2, default: 0.0
    t.decimal  "unit_rate",                         precision: 18, scale: 2, default: 0.0
    t.boolean  "tax",                                                        default: false
    t.decimal  "amount",                            precision: 18, scale: 2, default: 0.0
    t.string   "line_type",             limit: 255
    t.integer  "product_id",            limit: 4
    t.integer  "tax_account_id",        limit: 4
    t.decimal  "discount_percent",                  precision: 8,  scale: 2
    t.integer  "warehouse_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "purchase_line_item_id", limit: 4
  end

  create_table "purchase_return_sequences", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,             null: false
    t.integer  "purchase_return_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_return_taxes", force: :cascade do |t|
    t.integer  "purchase_return_line_item_id", limit: 4, null: false
    t.integer  "account_id",                   limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_returns", force: :cascade do |t|
    t.integer  "company_id",             limit: 4,                                            null: false
    t.integer  "purchase_id",            limit: 4,                                            null: false
    t.integer  "created_by",             limit: 4,                                            null: false
    t.string   "purchase_return_number", limit: 255
    t.integer  "account_id",             limit: 4,                                            null: false
    t.date     "record_date"
    t.text     "customer_notes",         limit: 65535
    t.decimal  "total_amount",                         precision: 18, scale: 2, default: 0.0
    t.decimal  "decimal",                              precision: 18, scale: 5, default: 0.0
    t.integer  "currency_id",            limit: 4
    t.decimal  "exchange_rate",                        precision: 18, scale: 5, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "warehouse_id",           limit: 4
    t.integer  "branch_id",              limit: 4
  end

  create_table "purchase_sequences", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,             null: false
    t.integer  "purchase_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_taxes", force: :cascade do |t|
    t.integer  "purchase_line_item_id", limit: 4, null: false
    t.integer  "account_id",            limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_warehouse_details", force: :cascade do |t|
    t.integer  "purchase_line_item_id", limit: 4
    t.integer  "warehouse_id",          limit: 4
    t.decimal  "quantity",                        precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",            limit: 4
    t.integer  "product_batch_id",      limit: 4
    t.boolean  "status_id",                                                default: true
    t.integer  "product_id",            limit: 4
    t.boolean  "deleted",                                                  default: false
  end

  create_table "purchases", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,                                              null: false
    t.integer  "created_by",                 limit: 4,                                              null: false
    t.string   "purchase_number",            limit: 255
    t.integer  "account_id",                 limit: 4
    t.date     "bill_date"
    t.date     "record_date"
    t.date     "due_date"
    t.string   "bill_reference",             limit: 255
    t.boolean  "deleted",                                                           default: false
    t.text     "customer_notes",             limit: 65535
    t.text     "terms_and_conditions",       limit: 65535
    t.integer  "deleted_by",                 limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",             limit: 255
    t.integer  "restored_by",                limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.integer  "project_id",                 limit: 4
    t.integer  "branch_id",                  limit: 4
    t.integer  "status_id",                  limit: 4
    t.string   "custom_field1",              limit: 255
    t.string   "custom_field2",              limit: 255
    t.string   "custom_field3",              limit: 255
    t.boolean  "stock_receipt",                                                     default: true
    t.decimal  "total_amount",                             precision: 18, scale: 2, default: 0.0
    t.integer  "currency_id",                limit: 4
    t.decimal  "exchange_rate",                            precision: 18, scale: 5, default: 0.0
    t.integer  "purchase_order_id",          limit: 4
    t.boolean  "tax_inclusive",                                                     default: false
    t.decimal  "outstanding",                              precision: 18, scale: 2
    t.integer  "settlement_account_id",      limit: 4
    t.decimal  "settlement_exchange_rate",                 precision: 18, scale: 5, default: 0.0
    t.decimal  "settled_amount",                           precision: 18, scale: 2, default: 0.0
  end

  create_table "purchases_payments", force: :cascade do |t|
    t.integer  "payment_voucher_id", limit: 4
    t.integer  "purchase_id",        limit: 4
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0
    t.decimal  "tds_amount",                   precision: 18, scale: 2, default: 0.0
    t.boolean  "deleted",                                               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "receipt_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,             null: false
    t.integer  "receipt_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "receipt_vouchers", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                              null: false
    t.integer  "invoice_id",        limit: 4
    t.integer  "created_by",        limit: 4,                                              null: false
    t.string   "voucher_number",    limit: 32,                                             null: false
    t.date     "voucher_date",                                                             null: false
    t.date     "received_date",                                                            null: false
    t.integer  "from_account_id",   limit: 4,                                              null: false
    t.integer  "to_account_id",     limit: 4,                                              null: false
    t.decimal  "amount",                          precision: 18, scale: 2, default: 0.0,   null: false
    t.text     "description",       limit: 65535
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id",        limit: 4
    t.integer  "branch_id",         limit: 4
    t.integer  "tds_account_id",    limit: 4
    t.decimal  "tds_amount",                      precision: 18, scale: 2, default: 0.0
    t.integer  "currency_id",       limit: 4
    t.decimal  "exchange_rate",                   precision: 18, scale: 5, default: 0.0
    t.boolean  "advanced",                                                 default: false
    t.boolean  "allocated"
  end

  create_table "receive_cashes", force: :cascade do |t|
    t.date     "received_date"
    t.decimal  "amount",                              precision: 10, scale: 2
    t.string   "receipt_mode",          limit: 255
    t.integer  "from_account_id",       limit: 4
    t.integer  "deposit_to_account_id", limit: 4
    t.string   "description",           limit: 255
    t.binary   "send_thank_you_mail",   limit: 65535
    t.integer  "cheque_number",         limit: 4
    t.date     "cheque_date"
    t.string   "bank",                  limit: 255
    t.string   "branch",                limit: 255
    t.integer  "transaction_id",        limit: 4
    t.date     "transaction_date"
    t.string   "type_of_credit_card",   limit: 255
    t.string   "card_number",           limit: 255
    t.integer  "company_id",            limit: 4
    t.integer  "user_id",               limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "receive_moneys", force: :cascade do |t|
    t.integer  "company_id",            limit: 4
    t.string   "voucher_number",        limit: 255,                                          null: false
    t.date     "received_date",                                                              null: false
    t.decimal  "amount",                              precision: 18, scale: 2, default: 0.0, null: false
    t.string   "receipt_mode",          limit: 255
    t.integer  "from_account_id",       limit: 4,                                            null: false
    t.integer  "deposit_to_account_id", limit: 4,                                            null: false
    t.string   "description",           limit: 255
    t.binary   "send_thank_you_mail",   limit: 65535
    t.integer  "user_id",               limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recursions", force: :cascade do |t|
    t.integer  "company_id",         limit: 4
    t.integer  "recursive_id",       limit: 4
    t.string   "recursive_type",     limit: 255
    t.date     "schedule_on"
    t.integer  "frequency",          limit: 4
    t.integer  "iteration",          limit: 4
    t.integer  "utilized_iteration", limit: 4
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recursive_invoices", force: :cascade do |t|
    t.integer  "invoice_id",   limit: 4
    t.integer  "recursion_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "invoice_date",           default: '2013-07-21'
  end

  create_table "reimbursement_note_attachments", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4,   null: false
    t.integer  "user_id",                    limit: 4,   null: false
    t.string   "reimbursement_note_id",      limit: 255, null: false
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reimbursement_note_line_items", force: :cascade do |t|
    t.integer  "reimbursement_note_id", limit: 4,                                            null: false
    t.text     "description",           limit: 65535
    t.decimal  "amount",                              precision: 18, scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "expense_account_id",    limit: 4,                                            null: false
  end

  create_table "reimbursement_note_sequences", force: :cascade do |t|
    t.integer  "company_id",                  limit: 4,             null: false
    t.integer  "reimbursement_note_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reimbursement_notes", force: :cascade do |t|
    t.integer  "company_id",                limit: 4,                                              null: false
    t.string   "reimbursement_note_number", limit: 255,                                            null: false
    t.date     "transaction_date",                                                                 null: false
    t.integer  "from_account_id",           limit: 4,                                              null: false
    t.integer  "branch_id",                 limit: 4
    t.text     "description",               limit: 65535
    t.decimal  "amount",                                  precision: 18, scale: 2, default: 0.0,   null: false
    t.boolean  "submitted",                                                        default: false
    t.integer  "created_by",                limit: 4,                                              null: false
    t.boolean  "deleted",                                                          default: false
    t.integer  "deleted_by",                limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",            limit: 255
    t.integer  "restored_by",               limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reimbursement_voucher_id",  limit: 4
    t.integer  "expense_id",                limit: 4
  end

  create_table "reimbursement_voucher_line_items", force: :cascade do |t|
    t.integer  "reimbursement_voucher_id", limit: 4,                                        null: false
    t.integer  "reimbursement_note_id",    limit: 4,                                        null: false
    t.decimal  "payment_amount",                     precision: 18, scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reimbursement_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",                     limit: 4,             null: false
    t.integer  "reimbursement_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reimbursement_vouchers", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                              null: false
    t.integer  "branch_id",         limit: 4
    t.integer  "created_by",        limit: 4,                                              null: false
    t.string   "voucher_number",    limit: 32,                                             null: false
    t.date     "voucher_date",                                                             null: false
    t.date     "received_date",                                                            null: false
    t.integer  "from_account_id",   limit: 4,                                              null: false
    t.integer  "to_account_id",     limit: 4,                                              null: false
    t.decimal  "amount",                          precision: 18, scale: 2, default: 0.0,   null: false
    t.text     "description",       limit: 65535
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reserves_and_surplus_accounts", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rights", force: :cascade do |t|
    t.string   "resource",   limit: 255
    t.string   "operation",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: :cascade do |t|
    t.integer  "plan_id",    limit: 4
    t.string   "name",       limit: 100,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc",       limit: 65535
  end

  create_table "saccounting_line_items", force: :cascade do |t|
    t.integer  "saccounting_id",  limit: 4,                                        null: false
    t.integer  "from_account_id", limit: 4,                                        null: false
    t.decimal  "amount",                    precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "saccounting_sequences", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,             null: false
    t.integer  "saccounting_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "saccounting_to_line_items", force: :cascade do |t|
    t.integer  "saccounting_id", limit: 4,                          null: false
    t.integer  "to_account_id",  limit: 4,                          null: false
    t.decimal  "amount",                   precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "saccountings", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                              null: false
    t.integer  "account_id",        limit: 4,                                              null: false
    t.integer  "created_by",        limit: 4,                                              null: false
    t.date     "voucher_date",                                                             null: false
    t.string   "voucher_number",    limit: 25,                                             null: false
    t.text     "description",       limit: 65535
    t.string   "tags",              limit: 255
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",         limit: 4
    t.decimal  "total_amount",                    precision: 18, scale: 2, default: 0.0
  end

  create_table "salaries", force: :cascade do |t|
    t.integer  "company_id",    limit: 4,                          null: false
    t.integer  "user_id",       limit: 4,                          null: false
    t.integer  "attendance_id", limit: 4,                          null: false
    t.integer  "payhead_id",    limit: 4,                          null: false
    t.decimal  "amount",                  precision: 18, scale: 2
    t.date     "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salary_computation_results", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,                                      null: false
    t.integer  "user_id",                  limit: 4,                                      null: false
    t.integer  "attendance_id",            limit: 4,                                      null: false
    t.integer  "payhead_id",               limit: 4,                                      null: false
    t.decimal  "amount",                             precision: 18, scale: 2
    t.date     "month",                                                                   null: false
    t.integer  "processed_by",             limit: 4
    t.integer  "status",                   limit: 4,                          default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id",               limit: 4,                                      null: false
    t.integer  "payroll_execution_job_id", limit: 4,                                      null: false
    t.integer  "payroll_detail_id",        limit: 4,                                      null: false
  end

  create_table "salary_structure_histories", force: :cascade do |t|
    t.integer  "salary_structure_id", limit: 4
    t.integer  "company_id",          limit: 4
    t.integer  "for_employee",        limit: 4
    t.integer  "created_by",          limit: 4
    t.date     "effective_from_date"
    t.date     "updated_on_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salary_structure_history_line_items", force: :cascade do |t|
    t.integer  "salary_structure_history_id", limit: 4
    t.integer  "payhead_id",                  limit: 4
    t.decimal  "amount",                                precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salary_structure_line_items", force: :cascade do |t|
    t.integer  "salary_structure_id", limit: 4,                                        null: false
    t.integer  "payhead_id",          limit: 4,                                        null: false
    t.decimal  "amount",                        precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salary_structures", force: :cascade do |t|
    t.integer  "company_id",          limit: 4
    t.integer  "for_employee",        limit: 4
    t.integer  "created_by",          limit: 4
    t.integer  "pay_head",            limit: 4
    t.integer  "pay_head_type",       limit: 4
    t.date     "effective_from_date"
    t.decimal  "amount",                        precision: 18, scale: 2
    t.decimal  "total",                         precision: 18, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "valid_till_date"
  end

  create_table "sales_accounts", force: :cascade do |t|
    t.boolean  "inventoriable"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description",   limit: 65535
    t.decimal  "unit_cost",                   precision: 10, scale: 2, default: 0.0
    t.decimal  "sell_cost",                   precision: 10, scale: 2, default: 0.0
  end

  create_table "sales_order_line_items", force: :cascade do |t|
    t.integer  "sales_order_id", limit: 4,                                            null: false
    t.integer  "product_id",     limit: 4
    t.integer  "account_id",     limit: 4
    t.integer  "tax_account_id", limit: 4
    t.decimal  "quantity",                     precision: 10, scale: 2
    t.decimal  "unit_rate",                    precision: 18, scale: 4
    t.decimal  "discount",                     precision: 4,  scale: 2
    t.text     "description",    limit: 65535
    t.string   "line_item_type", limit: 255
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales_order_sequences", force: :cascade do |t|
    t.integer  "company_id",           limit: 4
    t.integer  "sales_order_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales_order_taxes", force: :cascade do |t|
    t.integer  "sales_order_line_item_id", limit: 4, null: false
    t.integer  "account_id",               limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales_orders", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,                                            null: false
    t.integer  "account_id",           limit: 4
    t.integer  "created_by",           limit: 4,                                            null: false
    t.string   "voucher_number",       limit: 255
    t.date     "voucher_date",                                                              null: false
    t.integer  "status",               limit: 4,                              default: 4
    t.decimal  "total_amount",                       precision: 18, scale: 2, default: 0.0
    t.text     "customer_notes",       limit: 65535
    t.text     "terms_and_conditions", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "billing_status",       limit: 4,                              default: 0
    t.integer  "customer_id",          limit: 4
    t.integer  "branch_id",            limit: 4
    t.integer  "estimate_id",          limit: 4
    t.integer  "currency_id",          limit: 4
    t.decimal  "exchange_rate",                      precision: 18, scale: 5, default: 0.0
    t.integer  "project_id",           limit: 4
    t.string   "po_reference",         limit: 255
    t.date     "po_date"
  end

  create_table "sales_warehouse_details", force: :cascade do |t|
    t.integer  "invoice_line_item_id", limit: 4
    t.integer  "warehouse_id",         limit: 4
    t.decimal  "quantity",                       precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_batch_id",     limit: 4
    t.integer  "product_id",           limit: 4
    t.boolean  "draft",                                                   default: false
  end

  create_table "secured_loan_accounts", force: :cascade do |t|
    t.string   "account_number", limit: 50,                                        null: false
    t.string   "bank_name",      limit: 255,                                       null: false
    t.string   "loan_type",      limit: 255
    t.decimal  "interest_rate",              precision: 4, scale: 2, default: 0.0
    t.string   "details",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sharable_documents", force: :cascade do |t|
    t.integer  "company_id",                 limit: 4
    t.integer  "user_id",                    limit: 4
    t.integer  "folder_id",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uploaded_file_file_name",    limit: 255
    t.string   "uploaded_file_content_type", limit: 255
    t.integer  "uploaded_file_file_size",    limit: 4
    t.datetime "uploaded_file_updated_at"
  end

  add_index "sharable_documents", ["folder_id"], name: "index_sharable_documents_on_folder_id", using: :btree
  add_index "sharable_documents", ["user_id"], name: "index_sharable_documents_on_user_id", using: :btree

  create_table "shared_folders", force: :cascade do |t|
    t.integer  "company_id",     limit: 4
    t.integer  "user_id",        limit: 4
    t.string   "shared_email",   limit: 255
    t.integer  "shared_user_id", limit: 4
    t.integer  "folder_id",      limit: 4
    t.string   "message",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shared_folders", ["folder_id"], name: "index_shared_folders_on_folder_id", using: :btree
  add_index "shared_folders", ["shared_user_id"], name: "index_shared_folders_on_shared_user_id", using: :btree
  add_index "shared_folders", ["user_id"], name: "index_shared_folders_on_user_id", using: :btree

  create_table "simple_captcha_data", force: :cascade do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

  create_table "stock_histories", force: :cascade do |t|
    t.integer  "company_id",          limit: 4
    t.integer  "stock_id",            limit: 4
    t.integer  "financial_year_id",   limit: 4
    t.decimal  "opening_stock",                 precision: 18, scale: 2, default: 0.0
    t.decimal  "opening_stock_value",           precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_issue_line_items", force: :cascade do |t|
    t.integer  "stock_issue_voucher_id", limit: 4,                          null: false
    t.integer  "product_id",             limit: 4,                          null: false
    t.decimal  "quantity",                         precision: 10, scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_batch_id",       limit: 4
  end

  add_index "stock_issue_line_items", ["product_id"], name: "stk_issu_ln_item_product_idx", using: :btree

  create_table "stock_issue_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",                   limit: 4,             null: false
    t.integer  "stock_issue_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_issue_vouchers", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,   null: false
    t.integer  "warehouse_id",   limit: 4
    t.string   "voucher_number", limit: 255, null: false
    t.date     "voucher_date",               null: false
    t.integer  "issued_to",      limit: 4
    t.integer  "created_by",     limit: 4,   null: false
    t.string   "details",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",      limit: 4
    t.string   "custom_field1",  limit: 255
    t.string   "custom_field2",  limit: 255
    t.string   "custom_field3",  limit: 255
  end

  create_table "stock_ledgers", force: :cascade do |t|
    t.integer  "company_id",           limit: 4,                                          null: false
    t.integer  "product_id",           limit: 4,                                          null: false
    t.integer  "voucher_id",           limit: 4,                                          null: false
    t.string   "voucher_type",         limit: 255,                                        null: false
    t.integer  "voucher_line_item_id", limit: 4
    t.integer  "warehouse_id",         limit: 4,                                          null: false
    t.integer  "branch_id",            limit: 4
    t.datetime "transaction_date",                                                        null: false
    t.decimal  "credit_quantity",                  precision: 10,           default: 0
    t.decimal  "debit_quantity",                   precision: 10,           default: 0
    t.integer  "created_by",           limit: 4,                                          null: false
    t.decimal  "unit_price",                       precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stock_ledgers", ["branch_id"], name: "index_stock_ledgers_on_branch_id", using: :btree
  add_index "stock_ledgers", ["company_id"], name: "index_stock_ledgers_on_company_id", using: :btree
  add_index "stock_ledgers", ["product_id"], name: "index_stock_ledgers_on_product_id", using: :btree
  add_index "stock_ledgers", ["transaction_date"], name: "index_stock_ledgers_on_transaction_date", using: :btree
  add_index "stock_ledgers", ["warehouse_id"], name: "index_stock_ledgers_on_warehouse_id", using: :btree

  create_table "stock_receipt_line_items", force: :cascade do |t|
    t.integer  "stock_receipt_voucher_id", limit: 4,                                        null: false
    t.integer  "product_id",               limit: 4,                                        null: false
    t.decimal  "quantity",                           precision: 10, scale: 2,               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "unit_rate",                          precision: 18, scale: 2, default: 0.0
  end

  add_index "stock_receipt_line_items", ["product_id"], name: "stk_rcpt_ln_item_product_idx", using: :btree

  create_table "stock_receipt_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",                     limit: 4,             null: false
    t.integer  "stock_receipt_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_receipt_vouchers", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,   null: false
    t.integer  "warehouse_id",   limit: 4
    t.string   "voucher_number", limit: 255, null: false
    t.date     "voucher_date",               null: false
    t.integer  "received_by",    limit: 4
    t.string   "details",        limit: 255
    t.integer  "created_by",     limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",      limit: 4
    t.string   "custom_field1",  limit: 255
    t.string   "custom_field2",  limit: 255
    t.string   "custom_field3",  limit: 255
    t.integer  "purchase_id",    limit: 4
  end

  create_table "stock_transfer_line_items", force: :cascade do |t|
    t.integer  "stock_transfer_voucher_id", limit: 4,                          null: false
    t.integer  "product_id",                limit: 4,                          null: false
    t.decimal  "available_quantity",                  precision: 10, scale: 2
    t.decimal  "transfer_quantity",                   precision: 10, scale: 2, null: false
    t.integer  "destination_warehouse_id",  limit: 4,                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_batch_id",          limit: 4
  end

  create_table "stock_transfer_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",                      limit: 4,             null: false
    t.integer  "stock_transfer_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_transfer_vouchers", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,   null: false
    t.integer  "created_by",     limit: 4,   null: false
    t.integer  "warehouse_id",   limit: 4,   null: false
    t.string   "voucher_number", limit: 255, null: false
    t.string   "details",        limit: 255
    t.date     "transfer_date"
    t.date     "voucher_date"
    t.integer  "branch_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_wastage_line_items", force: :cascade do |t|
    t.integer  "stock_wastage_voucher_id", limit: 4,                              null: false
    t.integer  "product_id",               limit: 4,                              null: false
    t.decimal  "quantity",                               precision: 10, scale: 2
    t.text     "reason",                   limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_batch_id",         limit: 4
  end

  add_index "stock_wastage_line_items", ["product_id"], name: "stk_wast_ln_item_product_idx", using: :btree

  create_table "stock_wastage_voucher_sequences", force: :cascade do |t|
    t.integer  "company_id",                     limit: 4,             null: false
    t.integer  "stock_wastage_voucher_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stock_wastage_vouchers", force: :cascade do |t|
    t.integer  "company_id",     limit: 4,                     null: false
    t.integer  "warehouse_id",   limit: 4,                     null: false
    t.string   "voucher_number", limit: 255
    t.datetime "voucher_date"
    t.integer  "branch_id",      limit: 4
    t.integer  "created_by",     limit: 4,                     null: false
    t.boolean  "deleted",                      default: false
    t.integer  "deleted_by",     limit: 4
    t.string   "deleted_reason", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description",    limit: 65535
    t.string   "custom_field1",  limit: 255
    t.string   "custom_field2",  limit: 255
    t.string   "custom_field3",  limit: 255
  end

  create_table "stocks", force: :cascade do |t|
    t.integer  "company_id",               limit: 4,                                        null: false
    t.integer  "warehouse_id",             limit: 4,                                        null: false
    t.integer  "product_id",               limit: 4,                                        null: false
    t.decimal  "quantity",                           precision: 10, scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "opening_stock",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "opening_stock_unit_price",           precision: 10, scale: 2
  end

  add_index "stocks", ["product_id"], name: "stks_product_idx", using: :btree

  create_table "subscription_histories", force: :cascade do |t|
    t.integer  "company_id",              limit: 4
    t.integer  "plan_id",                 limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "renewal_date"
    t.datetime "first_subscription_date"
    t.string   "ip_address",              limit: 255
    t.decimal  "amount",                              precision: 18, scale: 2
    t.decimal  "allocated_storage_mb",                precision: 10
    t.integer  "allocated_user_count",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "company_id",              limit: 4
    t.integer  "plan_id",                 limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "status",                  limit: 255
    t.datetime "renewal_date"
    t.datetime "first_subscription_date"
    t.string   "ip_address",              limit: 255
    t.decimal  "amount",                              precision: 18, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "allocated_storage_mb",                precision: 10
    t.decimal  "utilized_storage_mb",                 precision: 10
    t.integer  "allocated_user_count",    limit: 4
    t.integer  "utilized_user_count",     limit: 4
    t.integer  "status_id",               limit: 4
    t.string   "token",                   limit: 255
  end

  create_table "sundry_creditors", force: :cascade do |t|
    t.string   "contact_number",     limit: 255
    t.string   "email",              limit: 255
    t.string   "PAN",                limit: 10
    t.string   "sales_tax_number",   limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tan",                limit: 25
    t.string   "vat_tin",            limit: 25
    t.string   "cst",                limit: 25
    t.string   "excise_reg_no",      limit: 25
    t.string   "service_tax_reg_no", limit: 25
  end

  create_table "sundry_debtors", force: :cascade do |t|
    t.string   "contact_number",           limit: 255
    t.string   "email",                    limit: 255
    t.string   "PAN",                      limit: 10
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tan",                      limit: 25
    t.string   "vat_tin",                  limit: 25
    t.string   "cst",                      limit: 25
    t.string   "excise_reg_no",            limit: 25
    t.string   "service_tax_reg_no",       limit: 25
    t.string   "website",                  limit: 255
    t.string   "fax",                      limit: 12
    t.string   "country",                  limit: 100
    t.string   "weekly_off",               limit: 50
    t.string   "cin_code",                 limit: 25
    t.string   "bank_name",                limit: 255
    t.string   "ifsc_code",                limit: 25
    t.string   "micr_code",                limit: 25
    t.string   "bsr_code",                 limit: 25
    t.integer  "credit_days",              limit: 4
    t.integer  "credit_limit",             limit: 4
    t.date     "date_of_incorporation"
    t.string   "open_time",                limit: 10
    t.string   "close_time",               limit: 10
    t.string   "notes",                    limit: 255
    t.integer  "product_pricing_level_id", limit: 4
  end

  create_table "super_users", force: :cascade do |t|
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.string   "username",        limit: 255
    t.string   "email",           limit: 255
    t.string   "hashed_password", limit: 255
    t.string   "salt",            limit: 255
    t.string   "role",            limit: 255
    t.boolean  "active"
    t.datetime "last_login"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "supports", force: :cascade do |t|
    t.integer  "company_id",             limit: 4,                     null: false
    t.string   "subject",                limit: 255
    t.text     "description",            limit: 65535
    t.integer  "created_by",             limit: 4,                     null: false
    t.date     "created_date"
    t.string   "assigned_to",            limit: 255
    t.integer  "status_id",              limit: 4,                     null: false
    t.date     "completed_date"
    t.string   "ticket_number",          limit: 255,                   null: false
    t.boolean  "deleted",                              default: false
    t.integer  "deleted_by",             limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",         limit: 255
    t.integer  "restored_by",            limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "resolution_description", limit: 65535
    t.integer  "reply_id",               limit: 4
  end

  create_table "suspense_accounts", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,   null: false
    t.integer  "user_id",          limit: 4,   null: false
    t.integer  "created_by",       limit: 4,   null: false
    t.integer  "assigned_to",      limit: 4,   null: false
    t.string   "description",      limit: 255
    t.date     "due_date"
    t.integer  "priority",         limit: 4
    t.integer  "task_status",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "billable"
    t.integer  "project_id",       limit: 4
    t.integer  "sales_account_id", limit: 4
    t.date     "completed_date"
  end

  create_table "tds_payment_vouchers", force: :cascade do |t|
    t.integer  "company_id",      limit: 4,                                            null: false
    t.integer  "created_by",      limit: 4,                                            null: false
    t.integer  "branch_id",       limit: 4
    t.date     "payment_date"
    t.string   "tan_no",          limit: 255,                                          null: false
    t.integer  "assessment_year", limit: 4,                                            null: false
    t.integer  "tds_account_id",  limit: 4,                                            null: false
    t.integer  "account_id",      limit: 4,                                            null: false
    t.string   "cin_no",          limit: 255
    t.string   "bsr_code",        limit: 255
    t.string   "challan_no",      limit: 255
    t.decimal  "basic_tax",                     precision: 18, scale: 2, default: 0.0
    t.decimal  "decimal",                       precision: 18, scale: 2, default: 0.0
    t.decimal  "surcharge",                     precision: 18, scale: 2, default: 0.0
    t.decimal  "edu_cess",                      precision: 18, scale: 2, default: 0.0
    t.decimal  "other",                         precision: 18, scale: 2, default: 0.0
    t.decimal  "penalty",                       precision: 18, scale: 2, default: 0.0
    t.decimal  "interest",                      precision: 18, scale: 2, default: 0.0
    t.text     "description",     limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "template_margins", force: :cascade do |t|
    t.integer  "template_id",      limit: 4
    t.integer  "hide_logo",        limit: 4,                default: 0
    t.integer  "hide_address",     limit: 4,                default: 0
    t.decimal  "top_margin",                 precision: 10, default: 10
    t.decimal  "left_margin",                precision: 10, default: 10
    t.decimal  "right_margin",               precision: 10, default: 20
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",       limit: 4,                             null: false
    t.integer  "HideRateQuantity", limit: 4,                default: 1
  end

  create_table "templates", force: :cascade do |t|
    t.string   "template_name", limit: 255
    t.string   "voucher_type",  limit: 255,                 null: false
    t.boolean  "deleted",                   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timesheet_line_items", force: :cascade do |t|
    t.integer  "timesheet_id", limit: 4,                null: false
    t.integer  "task_id",      limit: 4,                null: false
    t.decimal  "timestamp",              precision: 10
    t.date     "day"
    t.integer  "holiday_id",   limit: 4
    t.integer  "leave_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timesheets", force: :cascade do |t|
    t.integer  "company_id",  limit: 4, null: false
    t.integer  "user_id",     limit: 4, null: false
    t.date     "start_date"
    t.date     "end_date"
    t.date     "record_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transaction_sequences", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transfer_cash_sequences", force: :cascade do |t|
    t.integer  "company_id",             limit: 4,             null: false
    t.integer  "transfer_cash_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transfer_cashes", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,                                            null: false
    t.integer  "created_by",          limit: 4,                                            null: false
    t.date     "transaction_date",                                                         null: false
    t.decimal  "amount",                          precision: 18, scale: 2, default: 0.0
    t.integer  "transferred_from_id", limit: 4,                                            null: false
    t.integer  "transferred_to_id",   limit: 4,                                            null: false
    t.string   "description",         limit: 255
    t.string   "voucher_number",      limit: 255
    t.boolean  "deleted",                                                  default: false
    t.integer  "deleted_by",          limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",      limit: 255
    t.integer  "restored_by",         limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",           limit: 4
  end

  create_table "transfer_moneys", force: :cascade do |t|
    t.integer  "company_id",       limit: 4,                                          null: false
    t.string   "voucher_number",   limit: 255,                                        null: false
    t.date     "transaction_date",                                                    null: false
    t.decimal  "amount",                       precision: 18, scale: 2, default: 0.0, null: false
    t.integer  "from_account_id",  limit: 4,                                          null: false
    t.integer  "to_account_id",    limit: 4,                                          null: false
    t.string   "description",      limit: 255
    t.integer  "created_by",       limit: 4,                                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unsecured_loan_accounts", force: :cascade do |t|
    t.string   "entity_name",   limit: 255,                                       null: false
    t.string   "loan_type",     limit: 255
    t.decimal  "interest_rate",             precision: 4, scale: 2, default: 0.0
    t.string   "details",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_informations", force: :cascade do |t|
    t.integer  "user_id",              limit: 4,   null: false
    t.string   "gender",               limit: 255
    t.date     "birth_date"
    t.string   "emergency_contact",    limit: 255
    t.string   "marital_status",       limit: 255
    t.string   "passport_number",      limit: 255
    t.date     "passport_expiry_date"
    t.date     "marriage_date"
    t.string   "blood_group",          limit: 255
    t.string   "present_address",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permanent_address",    limit: 255
  end

  create_table "user_referrals", force: :cascade do |t|
    t.integer  "company_id",                limit: 4
    t.integer  "user_id",                   limit: 4
    t.integer  "referral_count",            limit: 4,                          default: 0
    t.integer  "paid_referral_count",       limit: 4,                          default: 0
    t.decimal  "referral_balance",                    precision: 18, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "registered_referral_count", limit: 4,                          default: 0
  end

  create_table "user_salary_details", force: :cascade do |t|
    t.integer  "user_id",             limit: 4,     null: false
    t.string   "work_type",           limit: 255
    t.integer  "status",              limit: 4
    t.text     "work_location",       limit: 65535
    t.string   "work_phone",          limit: 10
    t.date     "date_of_joining"
    t.string   "bank_account_number", limit: 20
    t.string   "branch",              limit: 255
    t.string   "bank_name",           limit: 255
    t.string   "PAN",                 limit: 255
    t.string   "EPS_account_number",  limit: 25
    t.string   "PF_account_number",   limit: 25
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "date_of_leaving"
    t.string   "employee_no",         limit: 255
    t.string   "payment_type",        limit: 255
    t.string   "ifsc_code",           limit: 255
  end

  create_table "usernotes", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "notes",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,                     null: false
    t.string   "username",            limit: 32,                    null: false
    t.string   "hashed_password",     limit: 255,                   null: false
    t.string   "salt",                limit: 255,                   null: false
    t.string   "first_name",          limit: 100,                   null: false
    t.string   "last_name",           limit: 100,                   null: false
    t.string   "email",               limit: 255,                   null: false
    t.integer  "department_id",       limit: 4
    t.integer  "designation_id",      limit: 4
    t.integer  "reporting_to_id",     limit: 4
    t.datetime "last_login_time"
    t.boolean  "deleted",                           default: false
    t.datetime "deleted_datetime"
    t.integer  "deleted_by",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
    t.boolean  "reset_password",                    default: false
    t.integer  "branch_id",           limit: 4
    t.string   "prefix",              limit: 255
    t.string   "middle_name",         limit: 255
    t.integer  "restored_by",         limit: 4
    t.datetime "restored_datetime"
    t.string   "remote_ip",           limit: 15
    t.integer  "feedback",            limit: 4
    t.text     "feedback_comment",    limit: 65535
    t.integer  "login_count",         limit: 4
  end

  create_table "variable_payhead_details", force: :cascade do |t|
    t.integer  "company_id", limit: 4,                                        null: false
    t.integer  "user_id",    limit: 4,                                        null: false
    t.integer  "payhead_id", limit: 4,                                        null: false
    t.decimal  "amount",               precision: 18, scale: 2, default: 0.0, null: false
    t.date     "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendor_imports", force: :cascade do |t|
    t.integer  "import_file_id",          limit: 4
    t.string   "name",                    limit: 255
    t.string   "opening_balance",         limit: 255
    t.string   "phone_number",            limit: 255
    t.string   "currency",                limit: 255
    t.string   "email",                   limit: 255
    t.string   "website",                 limit: 255
    t.string   "pan",                     limit: 255
    t.string   "tan",                     limit: 255
    t.string   "vat_tin",                 limit: 255
    t.string   "excise_reg_no",           limit: 255
    t.string   "service_tax_reg_no",      limit: 255
    t.string   "sales_tax_no",            limit: 255
    t.string   "lbt_registration_number", limit: 255
    t.string   "cst",                     limit: 255
    t.string   "billing_address",         limit: 255
    t.string   "city",                    limit: 255
    t.string   "state",                   limit: 255
    t.string   "country",                 limit: 255
    t.string   "postal_code",             limit: 255
    t.string   "shipping_address",        limit: 255
    t.integer  "created_by",              limit: 4
    t.integer  "status",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendors", force: :cascade do |t|
    t.integer  "company_id",              limit: 4,   null: false
    t.string   "name",                    limit: 255, null: false
    t.string   "email",                   limit: 255
    t.string   "pan",                     limit: 255
    t.string   "sales_tax_no",            limit: 255
    t.string   "tan",                     limit: 255
    t.string   "vat_tin",                 limit: 255
    t.string   "cst",                     limit: 255
    t.string   "excise_reg_no",           limit: 255
    t.string   "service_tax_reg_no",      limit: 255
    t.string   "website",                 limit: 255
    t.string   "incorporated_date",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",              limit: 4
    t.string   "lbt_registration_number", limit: 255
    t.string   "phone_number",            limit: 255
    t.integer  "currency_id",             limit: 4
  end

  create_table "voucher_settings", force: :cascade do |t|
    t.integer  "company_id",              limit: 4
    t.integer  "voucher_number_strategy", limit: 4
    t.string   "prefix",                  limit: 255
    t.string   "suffix",                  limit: 255
    t.integer  "voucher_type",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "customer_notes",          limit: 65535
    t.text     "terms_and_conditions",    limit: 65535
  end

  create_table "voucher_titles", force: :cascade do |t|
    t.integer  "company_id",    limit: 4,                  null: false
    t.string   "voucher_type",  limit: 255,                null: false
    t.string   "voucher_title", limit: 255
    t.boolean  "status",                    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vouchers", force: :cascade do |t|
    t.string   "voucher_number", limit: 255
    t.string   "type",           limit: 255
    t.date     "record_date"
    t.date     "due_date"
    t.text     "bill_reference", limit: 65535
    t.date     "bill_date"
    t.integer  "created_by",     limit: 4
    t.integer  "approved_by",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "warehouses", force: :cascade do |t|
    t.integer  "company_id", limit: 4,   null: false
    t.integer  "manager_id", limit: 4
    t.string   "name",       limit: 255
    t.string   "address",    limit: 255
    t.string   "phone",      limit: 255
    t.string   "city",       limit: 255
    t.string   "pincode",    limit: 255
    t.integer  "created_by", limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "webhook_payments", force: :cascade do |t|
    t.string   "payment_request_id", limit: 255,                                        null: false
    t.string   "payment_id",         limit: 255,                                        null: false
    t.string   "customer_name",      limit: 255
    t.string   "currency",           limit: 255,                                        null: false
    t.decimal  "amount",                         precision: 18, scale: 2, default: 0.0
    t.decimal  "fees",                           precision: 18, scale: 2, default: 0.0
    t.decimal  "decimal",                        precision: 18, scale: 2, default: 0.0
    t.string   "longurl",            limit: 255,                                        null: false
    t.string   "mac",                limit: 255,                                        null: false
    t.string   "status",             limit: 255,                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remote_ip",          limit: 100
  end

  create_table "webinars", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.date     "date"
    t.string   "city",       limit: 255
    t.text     "detail",     limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "withdrawal_sequences", force: :cascade do |t|
    t.integer  "company_id",          limit: 4,             null: false
    t.integer  "withdrawal_sequence", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "withdrawals", force: :cascade do |t|
    t.integer  "company_id",        limit: 4,                                            null: false
    t.integer  "created_by",        limit: 4,                                            null: false
    t.string   "voucher_number",    limit: 255
    t.date     "transaction_date"
    t.decimal  "amount",                        precision: 18, scale: 2, default: 0.0
    t.integer  "from_account_id",   limit: 4,                                            null: false
    t.integer  "to_account_id",     limit: 4,                                            null: false
    t.string   "description",       limit: 255
    t.boolean  "deleted",                                                default: false
    t.integer  "deleted_by",        limit: 4
    t.datetime "deleted_datetime"
    t.string   "deleted_reason",    limit: 255
    t.integer  "restored_by",       limit: 4
    t.datetime "restored_datetime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id",         limit: 4
  end

  create_table "workstreams", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.integer  "user_id",     limit: 4
    t.datetime "action_time"
    t.string   "IP_address",  limit: 100
    t.string   "action",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "action_code", limit: 255
    t.integer  "branch_id",   limit: 4
    t.integer  "customer_id", limit: 4
    t.integer  "vendor_id",   limit: 4
    t.integer  "project_id",  limit: 4
  end

  create_table "years", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

end
