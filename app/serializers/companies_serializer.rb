class CompanySerializer < ActiveModel::Serializer
  attributes :name, :activation_date, :phone
end
