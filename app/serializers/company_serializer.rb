class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :activation_date, :phone, :ca_status
end
