class CompaniesController < ApplicationController
  skip_before_action :verify_authenticity_token, if: :json_request?

  def index
    @_company = Company.last(10)
    render :json => ActiveModel::ArraySerializer.new(@_company, each_serializer: CompanySerializer, root: "company").to_json
  end

  def update
    @_company = Company.find(params[:id])
    @_company.update_attributes(company_params)
    render :json => CompanySerializer.new(@_company, root: "company").to_json
  end

  def company_params
    params.require(:company).permit(:ca_status)
  end

  protected
  def json_request?
    request.format.json?
  end
end
